#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
    char password[] = "qyupnvkrgzwzcsjh\0";

    int fd = open("/proc/pwncollege", O_WRONLY);

    write(fd, password, strlen(password));

    close(fd);

    exit(EXIT_SUCCESS);
}
