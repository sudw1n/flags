#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

int
main(void)
{
    char password[] = "bybrvaodnpgecgw\0";
    char buffer[BUFSIZ];

    int fd = open("/proc/pwncollege", O_RDWR);

    write(fd, password, strlen(password));

    read(fd, buffer, BUFSIZ);

    printf("%s\n", buffer);

    exit(EXIT_SUCCESS);
}
