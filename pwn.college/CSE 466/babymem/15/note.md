- First, you should try overflowing just the null byte of the canary, for
  practice. The canary starts at 0x7fff5e8d77a8, which is 24 bytes after the
  start of your buffer. Thus, you should provide 24 characters followed
  by a NULL byte, make sure the canary check passes, then try a non-NULL
  byte and make sure the canary check fails. This will confirm the offsets.
- Next try each possible value for just the next byte. One of them (the same
  as whatever was there in memory already) will keep the canary intact, and
  when the canary check succeeds, you know you have found the correct one.
- Go on to the next byte, leak it the same way, and so on, until you have
  the whole canary.
