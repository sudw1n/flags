/* source: https://yewtu.be/watch?v=5g137gsB9Wk */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>

int
main(int argc, char *argv[])
{
    while (true)
    {
        renameat2(AT_FDCWD, argv[1], AT_FDCWD, argv[2], RENAME_EXCHANGE);
    }

    return 0;
}
