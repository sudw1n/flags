import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    initial = b"\x81\x45\x29\xe0\xf7\x64\xed\x6e\xe5\xd8\x12\xb9\x2a\x60"
    mangle_key = b"\xce\x93\xaa\x1c\xae\xc9\x2c\xf3\xe0\x07\xca\xe0\xe5\xe2"
    password = bytearray()

    for i in range(len(initial)):
        password.append((initial[i] - mangle_key[i]) & 0xff)

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )
        io.send(password)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
