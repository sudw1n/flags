import pwn
import sys


if len(sys.argv) != 2:
    sys.stderr.write("Give the chall binary!\n")
    sys.exit(1)

pwn.warnings.simplefilter("ignore")

initial_password = bytearray(bytes.fromhex("aa96646a0bee6509a8216908"))
mangle_key = bytes.fromhex("1101cbc3949dd85433c70829")
password = bytearray()

for i in range(len(initial_password)):
    password.append( (initial_password[i] + mangle_key[i]) & 0xff )


with pwn.process(sys.argv[1]) as io:
    print( io.readrepeat(1).decode() )
    io.send(password)
    print( io.readrepeat(1).decode() )
