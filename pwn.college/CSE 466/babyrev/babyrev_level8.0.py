import pwn
import sys


def main(argv):
    if len(argv) < 2:
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    expected = bytearray( bytes.fromhex("27242424242121213f3f3e3d3d3c3b3b3b3a3a39393838383837373636363534333231") )

    # reverse the input
    expected = expected[::-1]

    xor_key = 0x50
    # xor mangler
    for i in range( len(expected) ):
        expected[i] ^= xor_key

    # sort mangler
    expected = bytearray( sorted(expected) )

    # swap indexes 10 and 28
    expected[28], expected[10] = expected[10], expected[28]

    # swap indexes 10 and 22
    expected[22], expected[10] = expected[10], expected[22]

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )

        io.send(expected)

        print( io.readrepeat(1).decode() )

    exit(0)

if __name__ == '__main__':
    # set some context
    pwn.context.arch = "amd64"
    pwn.context.encoding = "latin"
    # ignore warnings
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
