import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    password = b"\x65\xaf\xdd\xe4\x84\xdd\xe2\x40\xc5\x4d\x26\x6c\x27\x55\x25\x69\xb4\x95\x1f\xe5\x90"

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )
        io.send(password)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
