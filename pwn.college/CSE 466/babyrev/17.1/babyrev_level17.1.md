# My approach to solve this (dynamic analysis)

- give "_aaaaaaaaaaaaaaaaaaaaa_" as input
- check `CMP` values with `a` and `b`
- `b` will prolly have `actual_key`
- `a` will have input with `mangle_key` added
- subtract input with a to get `mangle_key`
- `actual_key` minus `mangle_key` should be given as input
