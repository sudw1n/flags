import pwn
import sys


def main(argv):
    if len(argv) < 2:
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    expected = bytearray( bytes.fromhex("64F3B19EFFFE63F7B5D5F9B062FAB995F6F76BFCBF93EDEB74E6A589EBE27DEBA986E7") )

    # xor index % 6 with key 22AFB8A0F2
    xor_key = b"\x22\xAF\xB8\xDB\xA0\xF2"
    for i, ch in enumerate(expected):
        expected[i] = ch ^ xor_key[i % 6]

    # swap index 9 and 11
    expected[9], expected[11] = expected[11], expected[9]

    # xor index % 3 with key 683D27
    xor_key = b"\x27\x3D\x68"
    for i, ch in enumerate(expected):
        expected[i] = ch ^ xor_key[i % 3]

    # swap index 6 and 10
    expected[6], expected[10] = expected[10], expected[6]

    expected = bytearray( sorted(expected) )

    # swap index 5 and 12
    expected[5], expected[12] = expected[12], expected[5]

    # reverse the input
    expected = expected[::-1]

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )

        io.send(expected)

        print( io.readrepeat(1).decode() )

    exit(0)

if __name__ == '__main__':
    # set some context
    pwn.context.arch = "amd64"
    pwn.context.encoding = "latin"
    # ignore warnings
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
