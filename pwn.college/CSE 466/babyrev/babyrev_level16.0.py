import pwn
import sys


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    # password = bytes.fromhex("faf94bbe59fd19aed7e5c6")
    password = b"\x4b\xbe\x59\xfd\x19\xae\xd7\xe5\xc6\xe5\xc6"

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )
        io.send(password)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
