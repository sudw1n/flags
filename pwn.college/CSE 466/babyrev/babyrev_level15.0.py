import pwn
import sys


if len(sys.argv) != 2:
    sys.stderr.write("Give the chall binary!\n")
    sys.exit(1)

pwn.warnings.simplefilter("ignore")

initial_password = bytearray(bytes.fromhex("c4a048910c81"))
mangle_key = bytes.fromhex("6ff2b7ef5f69")
password = bytearray()

for i in range(len(initial_password)):
    # only use the last 8 bits after the subtraction
    password.append( (initial_password[i] - mangle_key[i]) & 0xff )


with pwn.process(sys.argv[1]) as io:
    print( io.readrepeat(1).decode() )
    io.send(password)
    print( io.readrepeat(1).decode() )
