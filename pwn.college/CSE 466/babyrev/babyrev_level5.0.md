The challenge `xor`s the input with `0x22`. The reverse of Xor is Xor itself so
if we send the Xor's version of the license to the input, then the challenge
will xor it back to the original and we get the flag.
