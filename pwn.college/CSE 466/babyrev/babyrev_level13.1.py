import pwn
import sys


password = bytes.fromhex("d9de5c38cde1")

with pwn.process(sys.argv[1]) as io:
    print( io.readrepeat(1).decode() )
    io.send(password)
    print( io.readrepeat(1).decode() )
