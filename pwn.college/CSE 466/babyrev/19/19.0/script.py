import pwn
import sys


def get_opcodes():
    instructions = {
            "imm": 0x01,
            "jmp": 0x02,
            "stk": 0x10,
            "stm": 0x80,
            "cmp": 0x04,
            "ldm": 0x08,
            "sys": 0x20,
            "add": 0x40
    }

    syscalls = {
            "open": 0x80,
            "read": 0x08,
            "write": 0x20,
            "sleep": 0x40,
            "exit": 0x04
    }

    regs = {
            "s": 0x01,
            "a": 0x02,
            "b": 0x80,
            "c": 0x04,
            "d": 0x10,
            "f": 0x08,
            "i": 0x40
    }

    vm_code = open("yancode.asm", "r")
    opcodes = str()
    for line in vm_code.readlines():
        nonopcode = line.split(" ")

        instruction = instructions[nonopcode[0]]

        if "imm" in nonopcode[0]:
            arg1 = eval(nonopcode[2].strip("\n"))
        else:
            arg1 = regs[nonopcode[2].strip("\n")]

        if "sys" in nonopcode[0]:
            arg2 = syscalls[nonopcode[1]]
        else:
            arg2 = regs[nonopcode[1]]


        opcodes += f"{instruction:02x}{arg1:02x}{arg2:02x}"
    return opcodes


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    password = bytearray( bytes.fromhex( get_opcodes() ) )

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )
        io.send(password)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
