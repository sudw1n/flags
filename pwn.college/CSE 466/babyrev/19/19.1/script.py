import pwn
import sys


def get_opcodes():
    instructions = {
            "stm": 0x02,
            "imm": 0x10,
            "sys": 0x20,
            "stk": 0x08,
            "add": 0x40,
    }

    syscalls = {
            "exit": 0x10,
            "sleep": 0x04,
            "read": 0x01,
            "open": 0x02,
            "write": 0x20
    }

    regs = {
            "a": 0x80,
            "i": 0x08,
            "b": 0x01,
            "c": 0x02,
            "d": 0x04,
            "s": 0x10,
            "f": 0x20
    }

    vm_code = open("yancode.asm", "r")
    opcodes = str()
    for line in vm_code.readlines():
        nonopcode = line.split(" ")

        instruction = instructions[nonopcode[0]]

        if "sys" in nonopcode[0]:
            arg1 = syscalls[nonopcode[1]]
        else:
            arg1 = regs[nonopcode[1]]

        if "imm" in nonopcode[0]:
            arg2 = eval(nonopcode[2].strip("\n"))
        else:
            arg2 = regs[nonopcode[2].strip("\n")]

        if "stk" in nonopcode[0]:
            if nonopcode[1] in regs.keys():
                arg1 = regs[nonopcode[1]]
            else:
                arg1 = eval(nonopcode[1])

            if nonopcode[2].strip("\n") in regs.keys():
                arg2 = regs[nonopcode[2].strip("\n")]
            else:
                arg2 = eval(nonopcode[2].strip("\n"))


        opcodes += f"{arg2:02x}{instruction:02x}{arg1:02x}"
    return opcodes


def main(argv):
    if ( len(argv) != 2 ):
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    password = bytearray( bytes.fromhex( get_opcodes() ) )

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )
        io.send(password)
        print( io.readrepeat(1).decode() )

if __name__ == '__main__':
    pwn.warnings.simplefilter("ignore")

    main(sys.argv)
