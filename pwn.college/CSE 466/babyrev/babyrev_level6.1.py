import angr
import pwn
import os


# set some context for pwntools
pwn.context.arch = "amd64"
pwn.context.encoding = "latin"
# ignore warnings
pwn.warnings.simplefilter("ignore")

# challenge binary
chall = "/challenge/" + str(os.environ.get("HOSTNAME"))

def main():
    # load the project
    proj = angr.Project(chall)

    # create a SimulationManager
    simgr = proj.factory.simgr()

    # symbolically execute until we find a state that prints our flag
    simgr.explore(find=lambda s: b"You win! Here is your flag:" in s.posix.dumps(1))

    # get the required input out of the state
    required_input = simgr.found[0].posix.dumps(0)

    # launch the process with the input
    launch_chall(required_input)

    exit(0)

def launch_chall(required_input):
    with pwn.process(chall, level="CRITICAL") as io:
        pwn.info( io.readrepeat(1) )
        io.send( required_input )
        pwn.info( io.readrepeat(1) )

if __name__ == '__main__':
    main()
