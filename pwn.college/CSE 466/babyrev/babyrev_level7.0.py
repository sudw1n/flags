import pwn
import sys


def main(argv):
    if len(argv) < 2:
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    expected = bytearray(
            bytes.fromhex('155357156511415311691c4d5f1c6b18495a06710554570274015f4c0c')
            )
    xor_key = b"\x74\x27\x35\x76\x01"

    # xor mangler
    for i, ch in enumerate(expected):
        expected[i] = ch ^ xor_key[i % 5]

    # swap indexed 1 and 22
    expected[1], expected[22] = expected[22], expected[1]

    # expected is already sorted
    # sorted(expected)

    # swap indexed 12 and 20
    expected[12], expected[20] = expected[20], expected[12]

    # reverse
    expected = expected[::-1]

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )

        io.send(expected)

        print( io.readrepeat(1).decode() )

    exit(0)

if __name__ == '__main__':
    main(sys.argv)
