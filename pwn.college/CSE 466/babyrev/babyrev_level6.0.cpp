#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int
main(void)
{
    /* this is our actual license */
    string license = "xfzrhjllzlgzofsc";

    /* we work our way backward */

    // first swap index 1 and 15
    swap(license.at(1), license.at(15));

    // then swap index 0 and 9
    swap(license.at(0), license.at(9));

    // finally reverse the string
    reverse(license.begin(), license.end());

    cout << license << "\n";

    return 0;
}
