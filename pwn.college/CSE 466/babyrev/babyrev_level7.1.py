import pwn
import sys


def main(argv):
    if len(argv) < 2:
        sys.stderr.write("Give the chall binary!\n")
        exit(1)

    chall = argv[1]
    expected = bytearray( bytes("{vvetuurspwmkkkkhifgeleb`", "ascii") )

    # swap indexes 3 and 10
    expected[3], expected[10] = expected[10], expected[3]
    # swap indexes 10 and 21
    expected[3], expected[10] = expected[10], expected[3]

    xor_key = 0x01
    # xor with key 0x01
    for i in range( len(expected) ):
        expected[i] ^= xor_key

    # reverse the input
    expected = expected[::-1]

    with pwn.process(chall) as io:
        print( io.readrepeat(1).decode() )

        io.send(expected)

        print( io.readrepeat(1).decode() )

    exit(0)

if __name__ == '__main__':
    main(sys.argv)
