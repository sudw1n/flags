Things that I learned (painfully) while debugging:
- the binary has PIE enabled, and the base address is different on every start
- do the `start` then `info proc map` to find base address

The license needs to be equal to 'puwgl' but the challenge modifies our input so
we can't put in the license directly.

Performed `objdump` of the binary, and found that the input license is modified by the
following instructions:
```
1535:       0f b6 45 f5             movzx  eax,BYTE PTR [rbp-0xb]
1539:       88 45 f0                mov    BYTE PTR [rbp-0x10],al
153c:       0f b6 45 f6             movzx  eax,BYTE PTR [rbp-0xa]
1540:       88 45 f1                mov    BYTE PTR [rbp-0xf],al
1543:       0f b6 45 f1             movzx  eax,BYTE PTR [rbp-0xf]
1547:       88 45 f5                mov    BYTE PTR [rbp-0xb],al
154a:       0f b6 45 f0             movzx  eax,BYTE PTR [rbp-0x10]
154e:       88 45 f6                mov    BYTE PTR [rbp-0xa],al
```

Our input is located at `[rbp-0xe]`. Suppose I input `abcd`, then the buffer
will be as follows:

```
"abcd\n"
[rbp-0xe] = a
[rbp-0xd] = b
[rbp-0xc] = c
[rbp-0xb] = d
[rbp-0xa] = \n
```

From the disassembly above, it seems that the last two characters of the input
are swapped. And in fact, that was the case.

So the solution is obtained by giving `puwlg` as the input.
