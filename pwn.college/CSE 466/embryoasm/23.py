from pwn import *
import os

# set some context
context.arch = "amd64"
context.encoding = "latin"
# ignore warnings
warnings.simplefilter("ignore")

# assembly goes here, most of the rest is boilerplate
"""
most_common_byte(src_addr, size):
   b = 0
   i = 0
   for i <= size-1:
       curr_byte = [src_addr + i]
       [stack_base - curr_byte] += 1
   b = 0

   max_freq = 0
   max_freq_byte = 0
   for b <= 0xff:
       if [stack_base - b] > max_freq:
           max_freq = [stack_base - b]
           max_freq_byte = b

   return max_freq_byte
"""
assembly = """
push rbp
mov rbp, rsp
sub rsp, 256

mov r13, 0


loop:
    cmp r13, rsi

    je done

    xor rax, rax
    mov al, byte ptr [rdi+r13]

    mov rbx, rbp
    sub rbx, rax
    inc byte ptr [rbx]

    inc r13
    jmp loop

done:
    mov r13, 0
    mov r14, 0
    mov r15, 0

loop2:
    cmp r13, 256
    je done2

    mov rbx, rbp
    sub rbx, r13

    mov rax, 0
    mov al, byte ptr [rbx]
    cmp rax, r14
    jle not_greater

    mov r14, rax
    mov r15, r13

not_greater:
    inc r13
    jmp loop2

done2:
    mov rax, r15
    leave
    ret
"""

# the hostname will always be the same as challenge binary
with process(f"/challenge/{os.getenv('HOSTNAME')}") as target:
    info( target.readrepeat(1).decode() )
    target.send( asm(assembly) )
    info( target.readrepeat(1).decode() )
