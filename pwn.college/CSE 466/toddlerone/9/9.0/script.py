#!/usr/bin/env python3
from sys import argv, stderr
import pwn

def encode_instruction(instruction, arg1, arg2):
    instructions = {
            "imm": 0x01,
            "sys": 0x10,
            "stk": 0x80,
            "ldm": 0x40,
            "stm": 0x04
    }

    syscalls = {
            "open": 0x02,
            "read_memory": 0x10,
            "write": 0x20,
            "exit": 0x04
    }

    regs = {
            "a": 0x04,
            "b": 0x08,
            "c": 0x10,
            "d": 0x02,
            "s": 64,
            "i": 32,
            "f": 0x01,
            "NONE": 0x00
    }

    instruction_opcode = instructions[instruction]
    if "sys" in instruction:
        arg1_opcode = syscalls[arg1]
        arg2_opcode = regs[arg2]
    elif "stm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = regs[arg2]
    elif "imm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = eval(arg2)
    else:
        print("Undocumented instruction!", file=stderr, flush=True)
        exit(1)

    opcodes = f"{arg1_opcode:02x}{instruction_opcode:02x}{arg2_opcode:02x}"

    return bytes.fromhex( opcodes )


def read_asm_file(asm_file):
    opcodes = b""
    for line in asm_file.readlines():
        instruction_line = line.split(" ")
        instruction = instruction_line[0]
        arg1 = instruction_line[1]
        arg2 = instruction_line[2].strip("\n")

        opcodes += encode_instruction(instruction, arg1, arg2)

    return opcodes

if ( len(argv) != 2 ):
    print("Give the chall binary!\n", file=stderr, flush=True)
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "WARN"
pwn.warnings.simplefilter("ignore")

with pwn.process(argv[1], setuid=True) as io:
    asm_file = open("yancode.asm", "r")
    stage2 = open("yancode-stage2.asm", "r")
    opcodes = read_asm_file(asm_file)
    stage2_opcodes = b"\x00" * 19
    stage2_opcodes += read_asm_file(stage2)

    print(io.recvrepeat(timeout=0.1).decode())
    print("Sending (stage1):", opcodes)
    io.sendline(opcodes)
    print(io.recvrepeat(timeout=0.1).decode())
    print("Sending (stage2):", stage2_opcodes)
    io.sendline(stage2_opcodes)
    print(io.recvrepeat(timeout=0.1).decode())
    io.close()
    print(io.poll())
    # io.interactive()
