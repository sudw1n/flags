#!/usr/bin/env python3
import pwn
import re
from os import dup2
from tempfile import TemporaryFile
from sys import argv, stderr

def encode_instruction(instruction, arg1, arg2):
    instructions = {
            "imm": 0x10,
            "stm": 0x02,
            "sys": 0x40,
    }

    syscalls = {
            "open": 0x02|0x08|0x10,
            "read_memory": 0x08,
            "write": 0x10,
            "exit": 0x20,
    }

    regs = {
            "a": 0x10,
            "b": 0x20,
            "c": 0x02,
    }

    instruction_opcode = instructions[instruction]
    if "sys" in instruction:
        arg1_opcode = syscalls[arg1]
        arg2_opcode = regs[arg2]
    elif "imm" in instruction:
        arg1_opcode = regs[arg1]
        arg2_opcode = eval(arg2)
    elif re.search("imm|add|cmp|ldm|stm|stk", instruction):
        arg1_opcode = regs[arg1]
        arg2_opcode = regs[arg2]
    else:
        print("Undocumented instruction!", file=stderr, flush=True)
        exit(1)

    opcodes = f"{arg1_opcode:02x}{instruction_opcode:02x}{arg2_opcode:02x}"

    return bytes.fromhex( opcodes )


def read_asm_file(asm_file):
    opcodes = b""
    for line in asm_file.readlines():
        instruction_line = line.split(" ")
        instruction = instruction_line[0]
        arg1 = instruction_line[1]
        arg2 = instruction_line[2].strip("\n")

        opcodes += encode_instruction(instruction, arg1, arg2)

    return opcodes

if ( len(argv) != 2 ):
    print("Give the chall binary!\n", file=stderr, flush=True)
    exit(1)

pwn.context.arch = "amd64"
pwn.warnings.simplefilter("ignore")

# create a temporary file before the process is read and duplicate its fd to 0x37
# 0x37 being the length of the flag and hence how much will be read by read_memory. That's important
# because we're using the return value of read_memory for write
with TemporaryFile() as tmpfile:
    dup2(tmpfile.fileno(), 0x37)
    io = pwn.process(argv[1], close_fds=False)
    asm_file = open("yancode.asm", "r")
    opcodes = read_asm_file(asm_file)

    pwn.info("Sending payload")
    io.sendline(opcodes)
    io.recv(4096)
    io.close()
    tmpfile.seek(0)

    pwn.success(f"Flag: {tmpfile.read().decode()}")
    tmpfile.close()
