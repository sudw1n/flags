import scapy.all as scapy


SPORT = DPORT = SEQ = 31337

# setup the lower level headers
ether = scapy.Ether(src=scapy.get_if_hwaddr("eth0"))
ip = scapy.IP(src="11.0.0.2", dst="10.0.0.3")


# first send the SYN(seq=A)
tcp_syn = scapy.TCP(sport=SPORT, dport=DPORT, seq=SEQ, flags="S")
pkt = ether/ip/tcp_syn

# will receive SYN-ACK(seq=B, ack=A+1)
tcp_synack = scapy.srp1(pkt, iface="eth0")


# send ACK(seq=A+1, ack=B+1)
tcp_ack = scapy.TCP(sport=SPORT, dport=DPORT, seq=SEQ+1, ack=(tcp_synack["TCP"].seq)+1, flags="A")
pkt = ether/ip/tcp_ack

scapy.srp1(pkt, iface="eth0")
