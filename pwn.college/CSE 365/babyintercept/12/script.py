import scapy.all as scapy


scapy.conf.iface = "eth0"
src_mac = scapy.get_if_hwaddr(scapy.conf.iface)
src_ip = "10.0.0.2"
dst_ip = "10.0.0.3"

arp = scapy.ARP(op="is-at", hwsrc=src_mac, psrc=src_ip, pdst=dst_ip)

scapy.send(arp)
