.intel_syntax noprefix
.global _start

# macros
.set AF_INET, 2
.set SOCK_STREAM, 1
.set IPPROTO_IP, 0
.set BUFSIZ, 8192
.set O_RDONLY, 0
.set O_WRONLY, 1
.set O_CREAT, 0x40

# variables
.set sockaddr_size, 16
.set path_len, 16
.set msg_len, 19

# syscalls
.set SYS_read, 0
.set SYS_write, 1
.set SYS_open, 2
.set SYS_close, 3
.set SYS_socket, 41
.set SYS_accept, 43
.set SYS_bind, 49
.set SYS_listen, 50
.set SYS_fork, 57
.set SYS_exit, 60

_start:
    endbr64
    push rbp
    mov rbp, rsp


    mov edi, AF_INET
    mov esi, SOCK_STREAM
    mov edx, IPPROTO_IP
    call socket

    mov r12, rax

    sub rsp, sockaddr_size
    # sin_family
    mov word ptr [rbp-16], AF_INET
    # sin_port = 80
    mov word ptr [rbp-14], 0x5000
    # sin_addr = 0.0.0.0
    mov dword ptr [rbp-12], 0
    # pad
    mov qword ptr [rbp-8], 0

    mov rdi, r12
    mov rsi, rsp
    mov rdx, sockaddr_size
    call bind
    add rsp, sockaddr_size

    mov rdi, r12
    mov esi, 5
    call listen

    jmp loop


loop:
    endbr64
    sub rsp, 8

    mov rdi, r12
    xor esi, esi
    xor edx, edx
    call accept

    mov [rbp-8], rax

    mov eax, SYS_fork
    syscall

    cmp rax, 0
    je child

    mov rdi, [rbp-8]
    call close

    jmp loop

child:
    endbr64

    mov rdi, r12
    call close

    sub rsp, BUFSIZ

    mov rdi, [rbp-8]
    mov rsi, rsp
    mov rdx, BUFSIZ
    call read
    mov r12, rax

    mov r10, rsp

    cmp byte ptr [r10+3], 0x20
    je GET

    # skip the POST
    add r10, 5
    # add null byte after path
    mov byte ptr [r10+16], 0

    mov rdi, r10
    mov rsi, O_WRONLY
    or rsi, O_CREAT
    mov rdx, 0777
    call open
    push rax
    push rax

    sub r10, 5

    mov byte ptr [r10+16], 0x20

    mov rdi, r10
    mov rsi, r12
    call find_offset
    mov rbx, rax

    pop rdi
    mov rsi, r10
    add rsi, rbx
    sub r12, rbx
    mov rdx, r12
    call write

    pop rdi
    call close

    mov rdi, [rbp-8]
    lea rsi, message
    mov rdx, msg_len
    call write

    xor edi, edi
    call exit

find_offset:
    endbr64

    push rbp
    mov rbp, rsp

    sub rsp, 8

    mov qword ptr [rbp-8], 0

    # seek to the end
    add rdi, rsi

    jmp check_offset

check_offset:
    # 0xd = "\r"
    cmp byte ptr [rdi], 0xd
    je found_offset
    dec rdi

    mov rbx, [rbp-0x8]
    inc rbx
    mov [rbp-0x8], rbx

    jmp check_offset

found_offset:
    mov rax, [rbp-0x8]
    sub rsi, rax
    mov rax, rsi
    add rax, 2

    add rsp, 8

    leave
    ret

GET:
    endbr64

    # skip the GET
    add r10, 4
    # add null byte after path
    mov qword ptr [r10+16], 0

    mov rdi, r10
    mov rsi, O_RDONLY
    call open

    push rax

    pop rdi
    push rdi
    mov rsi, r10
    mov rdx, BUFSIZ
    call read
    push rax

    mov r13, rax

    call close

    mov rdi, [rbp-8]
    lea rsi, message
    mov rdx, msg_len
    call write

    mov rdi, [rbp-8]
    mov rsi, r10
    pop rdx
    call write

    mov rdi, [rbp-8]
    call close

    xor edi, edi
    call exit

socket:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_socket
    syscall

    leave
    ret

bind:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_bind
    syscall

    leave
    ret

listen:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_listen
    syscall

    leave
    ret

accept:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_accept
    syscall

    leave
    ret

read:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_read
    syscall

    leave
    ret

write:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_write
    syscall

    leave
    ret

open:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_open
    syscall

    leave
    ret

close:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_close
    syscall

    leave
    ret

exit:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_exit
    syscall

    leave
    ret

message:
    .string "HTTP/1.0 200 OK\r\n\r\n"
