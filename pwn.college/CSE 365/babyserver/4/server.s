.intel_syntax noprefix
.global _start

# macros
.set AF_INET, 2
.set SOCK_STREAM, 1
.set IPPROTO_IP, 0

# struct sizes
.set sockaddr_size, 16

# syscalls
.set SYS_socket, 41
.set SYS_exit, 60
.set SYS_bind, 49
.set SYS_listen, 50

_start:
    endbr64

    call socket
    push rax
    call bind
    pop rax
    call listen

    call exit

socket:
    endbr64
    push rbp
    mov rbp, rsp

    mov edi, AF_INET
    mov esi, SOCK_STREAM
    mov edx, IPPROTO_IP
    mov eax, SYS_socket
    syscall

    leave
    ret

bind:
    endbr64
    push rbp
    mov rbp, rsp

    sub rsp, sockaddr_size

    # sin_family
    mov word ptr [rbp-16], AF_INET
    # sin_port = 80
    mov word ptr [rbp-14], 0x5000
    # sin_addr = 0.0.0.0
    mov dword ptr [rbp-12], 0
    # pad
    mov qword ptr [rbp-8], 0

    mov rdi, rax
    mov rsi, rsp
    mov rdx, sockaddr_size
    mov rax, SYS_bind
    syscall

    add rsp, sockaddr_size

    leave
    ret

listen:
    endbr64
    push rbp
    mov rbp, rsp

    mov rdi, rax
    xor esi, esi
    mov eax, SYS_listen
    syscall

    leave
    ret

exit:
    endbr64
    push rbp
    mov rbp, rsp

    xor edi, edi
    mov eax, SYS_exit
    syscall

    leave
    ret
