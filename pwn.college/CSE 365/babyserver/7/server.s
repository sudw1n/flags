.intel_syntax noprefix
.global _start

# macros
.set AF_INET, 2
.set SOCK_STREAM, 1
.set IPPROTO_IP, 0
.set BUFSIZ, 8192
.set O_RDONLY, 0

# variables
.set sockaddr_size, 16
.set path_len, 16
.set msg_len, 19

# syscalls
.set SYS_read, 0
.set SYS_write, 1
.set SYS_open, 2
.set SYS_close, 3
.set SYS_socket, 41
.set SYS_accept, 43
.set SYS_bind, 49
.set SYS_listen, 50
.set SYS_exit, 60

_start:
    endbr64
    push rbp
    mov rbp, rsp


    mov edi, AF_INET
    mov esi, SOCK_STREAM
    mov edx, IPPROTO_IP
    call socket

    sub rsp, sockaddr_size
    # sin_family
    mov word ptr [rbp-16], AF_INET
    # sin_port = 80
    mov word ptr [rbp-14], 0x5000
    # sin_addr = 0.0.0.0
    mov dword ptr [rbp-12], 0
    # pad
    mov qword ptr [rbp-8], 0

    mov rdi, 3
    mov rsi, rsp
    mov rdx, sockaddr_size
    call bind
    add rsp, sockaddr_size

    mov rdi, 3
    xor esi, esi
    call listen

    mov rdi, 3
    xor esi, esi
    xor edx, edx
    call accept

    sub rsp, BUFSIZ

    mov edi, 4
    mov rsi, rsp
    mov rdx, BUFSIZ
    call read

    mov r10, rsp

    # skip the GET
    add r10, 4
    # add null byte after path
    mov qword ptr [r10+16], 0

    mov rdi, r10
    mov rsi, O_RDONLY
    call open

    mov edi, 5
    mov rsi, r10
    mov rdx, BUFSIZ
    call read

    push rax

    mov edi, 5
    call close

    mov edi, 4
    lea rsi, message
    mov rdx, msg_len
    call write

    mov edi, 4
    mov rsi, r10
    pop rdx
    call write

    mov rdi, 4
    call close

    xor edi, edi
    call exit

socket:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_socket
    syscall

    leave
    ret

bind:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_bind
    syscall

    leave
    ret

listen:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_listen
    syscall

    leave
    ret

accept:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_accept
    syscall

    leave
    ret

read:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_read
    syscall

    leave
    ret

write:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_write
    syscall

    leave
    ret

open:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_open
    syscall

    leave
    ret

close:
    endbr64
    push rbp
    mov rbp, rsp

    mov rax, SYS_close
    syscall

    leave
    ret

exit:
    endbr64
    push rbp
    mov rbp, rsp

    mov eax, SYS_exit
    syscall

    leave
    ret

message:
    .string "HTTP/1.0 200 OK\r\n\r\n"
