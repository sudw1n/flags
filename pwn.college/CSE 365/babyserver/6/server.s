.intel_syntax noprefix
.global _start

# macros
.set AF_INET, 2
.set SOCK_STREAM, 1
.set IPPROTO_IP, 0

# struct sizes
.set sockaddr_size, 16

# syscalls
.set SYS_socket, 41
.set SYS_exit, 60
.set SYS_bind, 49
.set SYS_listen, 50
.set SYS_accept, 43
.set SYS_write, 1
.set SYS_close, 3

_start:
    endbr64

    call socket

    mov r8, rax
    call bind

    mov rax, r8
    call listen

    mov rax, r8
    call accept
    mov r9, rax

    call write

    mov rax, r9
    call close

    call exit

socket:
    endbr64
    push rbp
    mov rbp, rsp

    mov edi, AF_INET
    mov esi, SOCK_STREAM
    mov edx, IPPROTO_IP
    mov eax, SYS_socket
    syscall

    leave
    ret

bind:
    endbr64
    push rbp
    mov rbp, rsp

    sub rsp, sockaddr_size

    # sin_family
    mov word ptr [rbp-16], AF_INET
    # sin_port = 80
    mov word ptr [rbp-14], 0x5000
    # sin_addr = 0.0.0.0
    mov dword ptr [rbp-12], 0
    # pad
    mov qword ptr [rbp-8], 0

    mov rdi, rax
    mov rsi, rsp
    mov rdx, sockaddr_size
    mov rax, SYS_bind
    syscall

    add rsp, sockaddr_size

    leave
    ret

listen:
    endbr64
    push rbp
    mov rbp, rsp

    mov rdi, rax
    xor esi, esi
    mov eax, SYS_listen
    syscall

    leave
    ret

accept:
    endbr64
    push rbp
    mov rbp, rsp

    mov rdi, rax
    xor esi, esi
    xor edx, edx
    mov eax, SYS_accept
    syscall

    leave
    ret

write:
    endbr64
    push rbp
    mov rbp, rsp

    mov rdi, rax
    lea rsi, message
    mov rdx, 19
    mov rax, SYS_write
    syscall

    leave
    ret

message:
    .ascii "HTTP/1.0 200 OK\r\n\r\n"

close:
    endbr64
    push rbp
    mov rbp, rsp

    mov rdi, rax
    mov rax, SYS_close
    syscall

    leave
    ret

exit:
    endbr64
    push rbp
    mov rbp, rsp

    xor edi, edi
    mov eax, SYS_exit
    syscall

    leave
    ret
