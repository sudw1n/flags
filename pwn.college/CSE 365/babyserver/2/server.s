.intel_syntax noprefix
.global _start

# macros
.set AF_INET, 2
.set SOCK_STREAM, 1
.set IPPROTO_IP, 0

# struct sizes
.set sockaddr_size, 16

# syscalls
.set SYS_socket, 41
.set SYS_exit, 60

_start:
    call socket
    call exit

socket:
    endbr64
    push rbp
    mov rbp, rsp

    mov edi, AF_INET
    mov esi, SOCK_STREAM
    mov edx, IPPROTO_IP
    mov eax, SYS_socket
    syscall

    leave
    ret

exit:
    endbr64
    push rbp
    mov rbp, rsp

    xor edi, edi
    mov eax, SYS_exit
    syscall

    leave
    ret
