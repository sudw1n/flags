import pwn


body = b"a=8bb4f0ffda07fcbf29d3f400440d9f5a";
head = str(f"POST / HTTP/1.0\r\nHost: localhost\r\nContent-Length: {len(body)}\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\n").encode()
payload = head+body

io = pwn.remote("localhost", 80)

pwn.info(f"Sending: {payload}")

io.send(payload)
print( io.readrepeat(1).decode() )
