import requests


response = b""
headers = {
        "Host": "localhost",
        "Cookie": ""
}

while b"pwn" not in response:
    response = requests.get("http://localhost", headers=headers)

    print(response.headers, response.text)

    cookie = str(input("Enter the cookie: "))
    headers["Cookie"] = f"Cookie: {cookie}"
