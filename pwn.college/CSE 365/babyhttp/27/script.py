from json import dumps
import pwn


json = {
        "a": "6d13a3d2b6b1c92f5a8888a674823b99"
}

body = dumps(json).encode()
head = f"POST / HTTP/1.0\r\nHost: localhost\r\nContent-Length: {len(body)}\r\nContent-Type: application/json\r\n\r\n".encode()
payload = head+body

io = pwn.remote("localhost", 80)

pwn.info(f"Sending: {payload}")

io.send(payload)
print( io.readrepeat(1).decode() )
