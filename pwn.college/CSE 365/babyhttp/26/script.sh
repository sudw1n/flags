content="$(cat data.json)"
content_len="$(wc -c data.json | cut -d' ' -f1)"
data="POST / HTTP/1.1\r\nContent-Type: application/json\r\nHost: localhost\r\nContent-Length: $content_len\r\n\r\n$content\r\n"

echo -ne "Sending: "; echo $data; echo

echo -ne "$data" | nc.openbsd localhost 80
