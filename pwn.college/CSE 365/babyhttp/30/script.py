from json import dumps
import pwn


json = {
        "a": "981f3888618d780016031bcd805d1d5c",
        "b":
            {
                "c": "af868a0a",
                "d": ["f2496984", "9f7dd249 9e833edb&d1704016#7600990f"]
            }

}

body = dumps(json).encode()
head = str(f"POST / HTTP/1.0\r\nHost: localhost\r\nContent-Length: {len(body)}\r\nContent-Type: application/json\r\n\r\n").encode()
payload = head+body

io = pwn.remote("localhost", 80)

pwn.info(f"Sending: {payload}")

io.send(payload)
print( io.readrepeat(1).decode() )
