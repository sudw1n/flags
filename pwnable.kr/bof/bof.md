* this is a 32-bit binary.
* 52-bytes after the input buffer, there's the key variable.
* Since this is using `gets()`, our job is very easy.
* payload = b"A" * 52 + pack(0xcafebabe)
* profit!
