* look for strings
* eventually, it will be revealed that the program is packed with `upx`
* unpack: `upx -d ./flag`
* step through `gdb` and break at where it performs the malloc
* look at the arguments when it makes the next function call
* `$rdi`: returned from malloc, `$rsi`: "UPX...? sounds like a delivery service :)"
