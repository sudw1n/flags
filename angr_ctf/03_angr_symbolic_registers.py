import angr
import claripy
import sys


def main(argv):
    proj = angr.Project(argv[1])
    # create an empty state with the address of the instruction after get_input
    # function is called
    state = proj.factory.blank_state(addr=0x08048980)

    # create symbolic bit vectors to represent the registers
    size = 32
    password0 = claripy.BVS('password0', size)
    password1 = claripy.BVS('password1', size)
    password2 = claripy.BVS('password2', size)

    state.regs.eax = password0
    state.regs.ebx = password1
    state.regs.edx = password2

    simgr = proj.factory.simgr(state)

    simgr.explore(
            find = lambda s: b'Good Job.' in s.posix.dumps(1),
            avoid = lambda s: b'Try again.' in s.posix.dumps(1) )

    if simgr.found:
        solution_state = simgr.found[0]

        solution0 = format(solution_state.solver.eval(password0), 'x')
        solution1 = format(solution_state.solver.eval(password1), 'x')
        solution2 = format(solution_state.solver.eval(password2), 'x')

        solution = solution0 + " " + solution1 + " " + solution2

        print("[+] Solution: %s" % solution)
    else:
        raise Exception("Couldn't find a solution")

if __name__ == '__main__':
    main(sys.argv)
