import angr
import sys

def main(argv):
    proj = angr.Project(argv[1])

    initial_state = proj.factory.entry_state()
    simgr = proj.factory.simgr(initial_state)

    # avoid the avoid_me address
    simgr.explore(find=0x080485DD, avoid=0x80485A8)

    if simgr.found:
        solution_state = simgr.found[0]
        solution = solution_state.posix.dumps(0)

        print( "[+] Solution: %s" % str(solution, 'utf-8') )
    else:
        raise Exception("Couldn't find solution")

if __name__ == '__main__':
    main(sys.argv)
