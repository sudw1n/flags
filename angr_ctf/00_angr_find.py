import angr
import sys

def main(argv):
    # open the project given from stdin
    proj = angr.Project(argv[1])

    # grab a state from its entry point
    initial_state = proj.factory.entry_state()
    # create a simulation manager out of the state
    simgr = proj.factory.simgr(initial_state)

    # find the address which prints the "Good Job." string
    simgr.explore(find=0x08048675)

    # if a state is found
    if simgr.found:
        # grab the first one
        solution_state = simgr.found[0]
        # grab which input angr provided as stdin for the simulation to reach
        # this state. This will be our solution.
        solution = solution_state.posix.dumps(0)

        # print the solution decoded as a string
        print( "[+] Solution: %s" % str(solution, 'utf-8') )
    else:
        raise Exception("Couldn't find solution")

if __name__ == '__main__':
    main(sys.argv)
