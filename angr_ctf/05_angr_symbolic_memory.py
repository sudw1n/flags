import angr
import claripy
import sys

def main(argv):
  path_to_binary = argv[1]
  project = angr.Project(path_to_binary)

  start_address = 0x08048601
  initial_state = project.factory.blank_state(
          addr=start_address,
          add_options = {
              angr.options.SYMBOL_FILL_UNCONSTRAINED_MEMORY,
              angr.options.SYMBOL_FILL_UNCONSTRAINED_REGISTERS
              }
          )

  password0 = claripy.BVS('password0', 8*8)
  password1 = claripy.BVS('password1', 8*8)
  password2 = claripy.BVS('password2', 8*8)
  password3 = claripy.BVS('password3', 8*8)

  password0_address = 0x0A1BA1C0
  initial_state.memory.store(password0_address,        password0)
  initial_state.memory.store(password0_address + 0x8,  password1)
  initial_state.memory.store(password0_address + 0x10, password2)
  initial_state.memory.store(password0_address + 0x18, password3)

  simgr = project.factory.simgr(initial_state)

  simgr.explore(
          find=lambda s: b'Good Job.\n' in s.posix.dumps(1),
          avoid=lambda s: b'Try again.\n' in s.posix.dumps(1) )

  if simgr.found:
    solution_state = simgr.found[0]

    solution0 = solution_state.solver.eval(password0, cast_to=bytes)
    solution1 = solution_state.solver.eval(password1, cast_to=bytes)
    solution2 = solution_state.solver.eval(password2, cast_to=bytes)
    solution3 = solution_state.solver.eval(password3, cast_to=bytes)

    solution = solution0 + b" " + solution1 + b" " + solution2 + b" " + solution3

    print(f"[+] Solution: {solution.decode()}")
  else:
    raise Exception('Could not find the solution')

if __name__ == '__main__':
  main(sys.argv)
