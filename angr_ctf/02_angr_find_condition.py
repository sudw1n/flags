import angr
import sys


# returns True is the binary prints 'Good Job.' to stdout for the given state
def is_successful(state):
    output = state.posix.dumps(1)
    return (b'Good Job.' in output)

# returns True is the binary prints 'Try again.' to stdout for the given state
def should_avoid(state):
    output = state.posix.dumps(1)
    return (b'Try again.' in output)

def main(argv):
    proj = angr.Project(argv[1])
    initial_state = proj.factory.entry_state()
    simgr = proj.factory.simgr(initial_state)

    simgr.explore(find=is_successful, avoid=should_avoid)

    if simgr.found:
        solution_state = simgr.found[0]
        solution = solution_state.posix.dumps(0)

        print("[+] Solution: %s" % str(solution, 'utf-8'))
    else:
        raise Exception("Couldn't find solution")

if __name__ == '__main__':
    main(sys.argv)
