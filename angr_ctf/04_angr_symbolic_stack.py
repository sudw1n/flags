import angr
import claripy
import sys


def main(argv):
    proj = angr.Project(argv[1])
    state = proj.factory.blank_state(addr=0x08048697)

    state.regs.ebp = state.regs.esp
    state.regs.esp -= 8

    password0 = claripy.BVS('password0', 32)
    password1 = claripy.BVS('password1', 32)

    state.stack_push(password0)
    state.stack_push(password1)

    simgr = proj.factory.simgr(state)

    simgr.explore(
            find = lambda s: b'Good Job.\n' in s.posix.dumps(1),
            avoid = lambda s: b'Try again.\n' in s.posix.dumps(1) )

    if simgr.found:
        solution_state = simgr.found[0]

        solution0 = solution_state.solver.eval(password0)
        solution1 = solution_state.solver.eval(password1)

        solution = str(solution0) + " " + str(solution1)

        print("[+] Solution: %s" % solution)
    else:
        raise Exception("Couldn't find solution")

if __name__ == '__main__':
    main(sys.argv)
