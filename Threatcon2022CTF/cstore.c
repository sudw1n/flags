#include <stdio.h>

int amount = 50;

void setup() {
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);
}

int main(int argc, char *argv[] ){
	setup();
	banner();
	while ( 1 ){
		show_wallet();
		menu();
	}

	return 0;
}

void banner(){
	puts("Welcome to the C-Store!");
	puts("");
}

int can_purchase(int price){
	if ( amount - price <= 0 ){
		printf("Sorry, you can't afford this! You only have $%d and this costs $%d.\n", amount, price);
		return 0;
	}else{
		puts("Congrats!");
		amount = amount - price;
		return 1;
	}
}

void show_wallet(){
	puts("======================================");
	printf("You have $%d dollars in your wallet.\n", amount);
}

void buy_menu(){
	puts("");
	puts("1) Buy the flag ($300000)");
	puts("2) Buy some integers ($4)");
	puts("3) Buy some chars ($1)");
	puts("4) Buy some null-bytes ($0)");
	puts("5) Go back");

	int input;
	scanf("%d", &input);

	switch(input){
		case 1:
			if ( can_purchase(300000) ){
				puts("You bought the flag!");
				system("/bin/cat flag.txt");
			}
			break;
		case 2:
			if ( can_purchase(4) ){
				puts("You bought some integers! Here you go: -1, 2, 3, tee hee!");
			}
			break;
		case 3:
			if ( can_purchase(1) ){
				puts("You bought some chars! Here you go: 'a', 'b', 'c', tee hee!");
			}
			break;
		case 4:
			if ( can_purchase(0) ){
				puts("You bought some null-bytes! Here you go--");
			}
			break;
		case 5:
			break;
		default:
			break;
	}
}

void sell_menu(){
	puts("");
	puts("1) Sell some integers");
	puts("2) Sell some chars");
	puts("3) Sell some null-bytes");
	puts("4) Go back");

	int input;
	scanf("%d", &input);

	switch(input){
		case 1:
			puts("Sorry, we don't need any more integers -- we have plenty in stock!");
			break;
		case 2:
			puts("Sorry, we don't need any more chars -- we have plenty in stock!");
			break;
		case 3:
			puts("How much do you want to sell the null-bytes for?");
			int price;
			scanf("%d", &price);
			if ( price > 0 ){
				puts("Sorry, I won't buy these for more than market price ($0)!");
			}else{
				printf("It's a deal! I'll buy them for $%d!\n", price);
				printf("You now have %d + %d in your wallet!\n", amount, price);
				amount = amount + price;
			}
			break;
		default:
			break;
	}
}

void menu(){

	puts("");	
	puts("1) Buy an item");
	puts("2) Sell an item");
	puts("3) Ask for a manager");
	puts("4) Exit the C-Store");

	int input;
	scanf("%d", &input);

	switch(input){
		case 1:
			buy_menu();
			break;
		case 2:
			sell_menu();
			break;
		case 3:
			puts("Sorry Karen, the manager is out today.");
			break;
		case 4:
			puts("Good-bye!");
			exit(0);
			break;
		default:
			break;
	}

}